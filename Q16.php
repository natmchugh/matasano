<?php

namespace Matasano;

require 'vendor/autoload.php';

$message = new Message();

$key = $message->genKey(16);
$iv = $message->genKey(16);

function encrypt($message, $key, $iv)
{
    $message = $message->prependAndQuote();
    $message = new Message($message);
    return $message->cbc_encrypt($key, $iv);
}

function isAdmin($message, $key, $iv)
{
    $decrypted = $message->cbc_decrypt($key, $iv);
    var_dump($decrypted);
    return 1 == preg_match('/;admin=true;/', $decrypted);
}

$message = new Message('test:admin<true');

$encrypted = encrypt($message, $key, $iv);

$fullString = $message->prependAndQuote();

//find position of : which is one byte less than ;
$position = strpos($fullString, ':admin');
//start in previous block
$targetPosition = $position - 16;
// XOR against difference in bits in plain test 
$encrypted[$targetPosition] = ($encrypted[$targetPosition] ^ 1);

//do the same for < to transform it to =
$position = strpos($fullString, '<true');
$targetPosition = $position - 16;
$encrypted[$targetPosition] = ($encrypted[$targetPosition] ^ 1);

// should now be admin
echo isAdmin($encrypted, $key, $iv) ? 'is admin': 'is not admin', PHP_EOL;

/*
output is:

string(89) "comment1=cookingE?t????r.j?]test;admin=true;comment2=%20like%20a%20pound%20of%20bacon"
is admin
*/