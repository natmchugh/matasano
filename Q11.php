<?php

namespace Matasano;

require 'vendor/autoload.php';

$message = new Message();
$input = 'YELLOW SUBMARINEYELLOW SUBMARINEYELLOW SUBMARINEYELLOW SUBMARINE';

$scores = array('ecb' => 0, 'cbc' => 0);
for ($i = 0; $i < 1000; $i++) {
    $cypherText = $message->encryption_oracle($input);

    $cypherMessage = new Message($cypherText);
    $analyser = new Analyser($cypherMessage);

    if ($analyser->containsDuplicates()) {
        $scores['ecb']++;
    } else {
        $scores['cbc']++;
    }
}

// should be roughly equal as depends on rand(1,2)
var_dump($scores);

/*
Output is:

array(2) {
  ["ecb"]=>
  int(507)
  ["cbc"]=>
  int(493)
}

 */