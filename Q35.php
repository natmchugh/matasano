<?php

function encrypt($msg, $p, $g, $s) {

    $iv = openssl_random_pseudo_bytes(16);
    $ct = openssl_encrypt($msg, 'aes-128-cbc', substr($s, 0, 16), OPENSSL_RAW_DATA, $iv);
    return $iv.$ct;
}

function Alice($sockets){

    $p = bi_base_convert('ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63'.
'b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b57'.
'6625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286'.
'651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c6'.
'2f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff', 16, 10);
    $g = 2;
    $a = bi_mod(mt_rand(), $p);
    $A = bi_powmod($g, $a, $p);
    socket_close($sockets[0]);

    //A->B            Send "p", "g"
    $toBob = json_encode([bi_to_str($p), $g]).PHP_EOL;
    socket_write($sockets[1], $toBob, strlen($toBob));

    // B->A            Send ACK
    $fromBob = socket_read($sockets[1], 1024, PHP_BINARY_READ);

    if ('ACK' != $fromBob) {
        die('failed to get ACK from Bob');
    }
    // A->B            Send "A"
    $toBob = json_encode([bi_to_str($A)]).PHP_EOL;
    socket_write($sockets[1], $toBob, strlen($toBob));
    
    //B->A            Send "B"
    $fromBob = socket_read($sockets[1], 1024, PHP_BINARY_READ);
    list($B) = json_decode($fromBob);
    $s = bi_to_str(bi_powmod($B, $a, $p));

    while (true) {
        $handle = fopen ("php://stdin","r");
        $msg = fgets($handle);
        // A->B            Send AES-CBC(SHA1(s)[0:16], iv=random(16), msg) + iv
        $ct = encrypt($msg, $p, $g, sha1($s));
        socket_write($sockets[1], $ct, strlen($ct));

        // B->A            Send AES-CBC(SHA1(s)[0:16], iv=random(16), A's msg) + iv
        $ct = socket_read($sockets[1], 1024, PHP_BINARY_READ);
        $pt = openssl_decrypt(substr($ct, 16), 'aes-128-cbc', substr(sha1($s), 0, 16), OPENSSL_RAW_DATA, substr($ct, 0, 16));
        echo 'Alice echos back: '.$pt;
        pcntl_wait($status);
    }
}

function Bob($sockets) {

        //A->B            Send "p", "g"
        $fromAlice = socket_read($sockets[0], 1024, PHP_BINARY_READ);
        list($p, $g) = json_decode($fromAlice);

        // B->A            Send ACK
        socket_write($sockets[0], 'ACK', strlen('ACK'));

        // A->B            Send "A"
        $fromAlice = socket_read($sockets[0], 1024, PHP_BINARY_READ);
        list($A) = json_decode($fromAlice);


        $b = bi_mod(mt_rand(), $p);
        $B = bi_powmod($g, $b, $p);
        // B->A            Send "B"
        $toAlice = json_encode([bi_to_str($B)]).PHP_EOL;
        socket_write($sockets[0], $toAlice, strlen($toAlice));
        $s = bi_to_str(bi_powmod($A, $b, $p));
    while (true) {
        // A->B            Send AES-CBC(SHA1(s)[0:16], iv=random(16), msg) + iv
        $ct = socket_read($sockets[0], 1024, PHP_BINARY_READ);
        $msg = openssl_decrypt(substr($ct, 16), 'aes-128-cbc', substr(sha1($s), 0, 16), OPENSSL_RAW_DATA, substr($ct, 0, 16));

        // B->A            Send AES-CBC(SHA1(s)[0:16], iv=random(16), A's msg) + iv
        $ct = encrypt($msg, $p, $g, sha1($s));
        socket_write($sockets[0], $ct, strlen($ct));
    }
}

function Maurice($sockets)
{
    list($alicePair, $bobPair) = $sockets;
    //A->M            Send "p", "g"
    $fromAlice = socket_read($alicePair[0], 1024, PHP_BINARY_READ);
    list($p, $p) = json_decode($fromAlice);
    //M->B            Send "p", "g"
    $toBob = json_encode([$p, 1]);
    socket_write($bobPair[1], $toBob, strlen($toBob));

    // B->A            Send ACK
    $fromBob = socket_read($bobPair[1], 1024, PHP_BINARY_READ);
    socket_write($alicePair[0], $fromBob, strlen($fromBob));

    // A->B            Send "A"
    $fromAlice = socket_read($alicePair[0], 1024, PHP_BINARY_READ);
    socket_write($bobPair[1], $fromAlice, strlen($fromAlice));

    // B->A            Send "B"
    $fromBob = socket_read($bobPair[1], 1024, PHP_BINARY_READ);
    socket_write($alicePair[0], $fromBob, strlen($fromBob));

    while (true) {
        // A->M            Send AES-CBC(SHA1(s)[0:16], iv=random(16), msg) + iv
        $fromAlice = socket_read($alicePair[0], 1024, PHP_BINARY_READ);
        // M->B            Relay that to B
        socket_write($bobPair[1], $fromAlice, strlen($fromAlice));

        // B->M            Send AES-CBC(SHA1(s)[0:16], iv=random(16), A's msg) + iv
        $fromBob = socket_read($bobPair[1], 1024, PHP_BINARY_READ);

        //just send back what Alice sent 
        //M->A            Relay that to A
        socket_write($alicePair[0], $fromAlice, strlen($fromAlice));

        $key = substr(sha1(0), 0, 16);
        $msg = openssl_decrypt(substr($fromAlice, 16), 'aes-128-cbc', $key, OPENSSL_RAW_DATA, substr($fromAlice, 0, 16));
        echo 'Maurice stole message: '.$msg;
    }

}

$children = $sockets = $pair = array();
socket_create_pair(AF_UNIX, SOCK_STREAM, 0, $pair);
$sockets[] = $pair;
socket_create_pair(AF_UNIX, SOCK_STREAM, 0, $pair);
$sockets[] = $pair;
for ($i = 0; $i < 3; $i++) {
    $children[] = $pid = pcntl_fork();
    if (0 === $pid) {
        echo 'Starting ';
        switch($i) {
            case 0:
                echo 'Alice'.PHP_EOL;
                Alice($sockets[0]);
                die();
            break;
            case 1:
                echo 'Bob'.PHP_EOL;
                Bob($sockets[1]);
                die();
            break;
            case 2:
                echo 'Maurice'.PHP_EOL;
                Maurice($sockets);
                die();
            break;
        }
    }
}

do {
    $pid = pcntl_wait($status);
    $children = array_diff($children, array($pid));
} while(count($children) > 0);
