<?php

namespace Matasano;

require 'vendor/autoload.php';

$lyrics = <<< ICE
Burning 'em, if you ain't quick and nimble
I go crazy when I hear a cymbal
ICE;


$message = new Message($lyrics);
$xorEncoder = new xorEncoder($message, 'ICE');
echo $xorEncoder->encode()->toHex(),PHP_EOL;