<?php

namespace Matasano;

require 'vendor/autoload.php';

$message = new Message('YELLOW SUBMARINE');

// pad to 20 bytes per block should see 4 X 04
$blocks = $message->pks7pad(20);

foreach ($blocks as $block) {
    echo $block->toHex(), PHP_EOL;
}

/**
 * Output is:
 *
 * 
 * 59454c4c4f57205355424d4152494e4504040404
 */


// pad to 16 bytes per block should see 16 X 10
$blocks = $message->pks7pad(16);

foreach ($blocks as $block) {
    echo $block->toHex(), PHP_EOL;
}

/**
 * Output is:
 *
 * 59454c4c4f57205355424d4152494e45
 * 10101010101010101010101010101010
 */