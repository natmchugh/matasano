<?php

namespace Matasano;

require 'vendor/autoload.php';

function getMT19937()
{
    $firstWait = rand(40, 1000);
    echo 'waiting',PHP_EOL;
    sleep($firstWait);
    echo 'seeding',PHP_EOL;
    $mt = new MersenneTwister(time());
    $secondWait = rand(40, 1000);
    echo 'waiting again',PHP_EOL;
    sleep($secondWait);
    return $mt->extract_number();
}

$firstRNG = getMT19937();
echo $firstRNG,PHP_EOL;

guessSeed($firstRNG);

function guessSeed($rng)
{
    echo 'Guessing Seed',PHP_EOL;
    $timeNow = time();
    echo 'Time now:',$timeNow,PHP_EOL;
    $start = $timeNow - 2000;
    for ($i = $start; $i <= $timeNow; $i++) {
        $mt = new MersenneTwister($i);
        if ($mt->extract_number() == $rng) {
            echo 'Seed Found:',$i,PHP_EOL;
        }
    }
}

/*
    Example Output:

waiting
seeding
waiting again
3040273226
Guessing Seed
Time now:1394702913
Seed Found:1394702689
    
 */
