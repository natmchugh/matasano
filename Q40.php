<?php

namespace Matasano;

require 'vendor/autoload.php';

$plainText = 'All right stop, Collaborate and listen';

$exponents = $mods = $cts = array();
for ($i =  0; $i < 3; $i++) {
    $rsa = new RSA();
    $ct = $rsa->encrypt($plainText);
    list($e, $n) = $rsa->getPublicKey();
    $exponents[] = $e;
    $cts[] = $ct;
    $ns[] =$n;
}

/*

 result =
   (c_0 * m_s_0 * invmod(m_s_0, n_0)) +
   (c_1 * m_s_1 * invmod(m_s_1, n_1)) +
   (c_2 * m_s_2 * invmod(m_s_2, n_2)) mod N_012

 */
$ms0 = bi_mul($ns[1], $ns[2]);
$ms1 = bi_mul($ns[0], $ns[2]);
$ms2 = bi_mul($ns[0], $ns[1]);

$invMod0 = NumberTheory::invMod($ms0, $ns[0]);
$invMod1 = NumberTheory::invMod($ms1, $ns[1]);
$invMod2 = NumberTheory::invMod($ms2, $ns[2]);

$t0 = bi_mul($cts[0], bi_mul($ms0, $invMod0));
$t1 = bi_mul($cts[1], bi_mul($ms1, $invMod1));
$t2 = bi_mul($cts[2], bi_mul($ms2, $invMod2));

$n012 = bi_mul($ns[2], bi_mul($ns[0], $ns[1]));

$result = bi_addmod($t0, bi_add($t1, $t2), $n012);
$m = NumberTheory::cubeRoot(bi_to_str($result));

var_dump(bi_to_str($m));
$length = ceil(strlen($m) / 3) * 3;
$m = str_pad($m, $length, '0', STR_PAD_LEFT);
$M = new Message($m, Message::DECIMAL);
var_dump((string) $M);
