<?php

namespace Matasano;

require 'vendor/autoload.php';

use Guzzle\Http\Client;
use Guzzle\Http\Exception\RequestException;

function callOracle($cipherText) {
    $client = new Client('http://crypto-class.appspot.com/');
    $request = $client->get(array('po{?er}', array('er' => $cipherText)));
    try {
        $response = $request->send();
    } catch (RequestException $badResponse) {
        return $badResponse->getResponse()->getStatusCode() == 403? 2: 1;
    }
    return 0;
}

$targetString = 'f20bdba6ff29eed7b046d1df9fb7000058b1ffb4210a580f748b4ac714c001'.
    'bd4a61044426fb515dad3f21f18aa577c0bdf302936266926ff37dbf7035d5eeb4';

$blocks = str_split(substr($targetString, 0), 32);


$plainTexts = array_pad(array(), 3, array());


// add 0x01 to previous elments for last block if this doesm't invalidate padding bingo
function incrementPadding($c1Prime, $c1, $c2) {
    for ($k = 1; $k < 17; $k++) {
        for ($l =0; $l < $k; $l++) {
            $c1Prime[15 - $l] = ($c1[15  -$l] ^ $k);
        }
        $code = callOracle($c1Prime->toHex().$c2->toHex());
        if ($code < 2) {
            return $k + 1;
        }
    }
    return false;
}

function testPenultimateChr($c1, $c2) {
    global $key;
    $c1Prime = clone($c1);
    // try all xors except 0 against penultimate chr if we find one that also gives valid padding we had 0x01
    $ords = range(0x01, 0xff);
    foreach ($ords as $ord) {
        $c1Prime[14] = ($c1[14] ^ $ord);
        $code = callOracle($c1Prime->toHex().$c2->toHex());
        if ($code < 2) {
            return false;
        }
    }

    return true;
}

// get a prioritised list of ords we could decrypt to
function getOrds($blockNo, $posInBlock, $c1n) {
    if ($blockNo == 2 && $posInBlock == 16) {
        $priority = range(0x00, 0xff);
    } else {
        $priority = array_merge(array(32), range(97, 122), range(65, 96), range(0, 31), range(33, 64), range(123, 255) );
        
    }
    $ords = array();
    foreach ($priority as $p2) {
        $ords[] = ($p2 ^ (1 ^ $c1n));
    }
    return $ords;
}
$blockNo = 0;

while ($blockNo < 3) {
    $decoded = array();
    $padding = '';
    $c1 = new Message($blocks[$blockNo], Message::HEX);
    $c1Prime = clone($c1);
    $c2 = new Message($blocks[$blockNo + 1], Message::HEX);
    for ($j = 1 ; $j < 17; $j++) {
        $ords = getOrds($blockNo, 17 - $j, $c1[16 - $j]);
        foreach ($ords as $i) {
            $c1Prime[16 - $j] = $i;
            $code = callOracle($c1Prime->toHex().$padding.$c2->toHex());
            if ($code < 2)  {
                if ($blockNo == 2 && $j == 1) {
                    if (testPenultimateChr($c1Prime, $c2)) {
                        continue 1;
                    }
                }
                $i2 = ($i ^ $j);
                $lastByte = ($i2 ^ $c1[16 - $j]);
                array_unshift($plainTexts[$blockNo], chr($lastByte));
                array_unshift($decoded, $i2);
                var_dump(chr($lastByte));
                $padding = '';
                foreach ($decoded as $decode) {
                    $padding .= sprintf('%02x', (($j + 1) ^ $decode));
                }
                $c1Prime = new Message(substr($c1, 0, 15 - $j));
                break 1;
            }
        }
    }
    $blockNo++;
}
ksort($plainTexts);
foreach ($plainTexts as $plainText) {
    echo implode($plainText);
}
echo PHP_EOL;

