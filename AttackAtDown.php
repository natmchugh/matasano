<?php

namespace Matasano;

require 'vendor/autoload.php';

$hex = '09e1c5f70a65ac519458e7e53f36';

$message = new Message($hex, Message::HEX);
$plainText = new Message('attack at dawn');

$encoder = new XorEncoder($message, $plainText);
$key = $encoder->encode();

$newPlainText = new Message('attack at dusk');
$encoder = new XorEncoder($newPlainText, $key);

$cipherText = $encoder->encode();
var_dump($cipherText->toHex());