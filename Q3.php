<?php

namespace Matasano;

require 'vendor/autoload.php';

$hex = '1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736';
$message = new Message($hex, Message::HEX);
$xorEncoder = new XorEncoder($message);
$key = $xorEncoder->getBestKey();
$xorEncoder->setKey($key);
echo "Key is $key",PHP_EOL;
echo $xorEncoder->encode(),PHP_EOL;

