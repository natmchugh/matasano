<?php

namespace Matasano;

require 'vendor/autoload.php';

$json = json_encode(['time' => time(), 'social' => '555-55-5555']);
$message = new Message($json);

$rsa = new RSA();

$c = $rsa->encrypt($message);

class DecryptServer {

    private $hashesAlreadySeen = array();

    function __invoke($rsaBlob, $rsa) {
        $hash = hash('sha256', bi_to_str($rsaBlob));
        if (in_array($hash, $this->hashesAlreadySeen)) {
            throw new \BadMethodCallException('This message is out of date');
        }
        $this->hashesAlreadySeen[] = $hash;
        return $rsa->decrypt($rsaBlob);
    }
}

$server = new DecryptServer();

// decrypt once to prevent decrypt again of same message
$p = $server($c, $rsa);

list($e, $n) = $rsa->getPublicKey();
$s = mt_rand(1, 100);

// C' = ((S**E mod N) * C) mod N
$cPrime = bi_mulmod(bi_powmod($s, $e, $n), $c, $n);
$pPrime = $server($cPrime, $rsa);

$pPrimeAsStr = $pPrime->toDecimal();
$pPrimeAsInt = bi_from_str(ltrim($pPrimeAsStr, '0'));
//       P'
// P = -----  mod N
//       S

$p = bi_mulmod($pPrimeAsInt, NumberTheory::invMod($s, $n), $n);
$p = bi_to_str($p);
// $length = ceil(strlen($p) / 3) * 3;
// $p = str_pad($p, $length, '0', STR_PAD_LEFT);
echo new Message($p, Message::DECIMAL),PHP_EOL;