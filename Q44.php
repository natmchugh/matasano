<?php

namespace Matasano;

require 'vendor/autoload.php';

$url = 'https://gist.githubusercontent.com/anonymous/f83e6b6e6889f2e8b7ff/raw/'.
    '3eea46b071376679c9cde13dd27c4fb22a56e601/challenge%2044';

$y = '2d026f4bf30195ede3a088da85e398ef869611d0f68f07'.
      '13d51c9c1a3a26c95105d915e2d8cdf26d056b86b8a7b8'.
      '5519b1c23cc3ecdc6062650462e3063bd179c2a6581519'.
      'f674a61f1d89a1fff27171ebc1b93d4dc57bceb7ae2430'.
      'f98a6a4d83d8279ee65d71c1203d2c96d65ebbf7cce9d3'.
      '2971c3de5084cce04a2e147821';

$p = bi_base_convert('800000000000000089e1855218a0e7dac38136ffafa72eda7'.
 '859f2171e25e65eac698c1702578b07dc2a1076da241c76c6'.
 '2d374d8389ea5aeffd3226a0530cc565f3bf6b50929139ebe'.
 'ac04f48c3c84afb796d61e5a4f9a8fda812ab59494232c7d2'.
 'b4deb50aa18ee9e132bfa85ac4374d7f9091abc3d015efc87'.
 '1a584471bb1', 16 ,10);

$q = bi_base_convert('f4f47f05794b256174bba6e9b396a7707e563c5b', 16, 10);

$g = bi_base_convert('5958c9d3898b224b12672c0b98e06c60df923cb8bc999d119'.
 '458fef538b8fa4046c8db53039db620c094c9fa077ef389b5'.
 '322a559946a71903f990f1f7e0e025e2d7f7cf494aff1a047'.
 '0f5b64c36b625a097f1651fe775323556fe00b3608c887892'.
 '878480e99041be601a62166ca6894bdd41a7054ec89f756ba'.
 '9fc95302291', 16, 10);

$lines = file($url);

$msgs = $sValues = $rValues = $mValues = array();

foreach ($lines as $line) {
    list($key, $value) = explode(':', $line);
    $value = trim($value);
    switch ($key) {
        case 'msg':
            $msgs[] = sprintf('%s ', $value);
        break;
        case 's':
            $sValues[] = $value;
        break;
        case 'r':
            $rValues[] = $value;
        break;
        case 'm':
        $mValues[] = bi_base_convert($value, 16, 10);
        break;
    }
}

function compareSignatures($m1, $m2, $s1, $s2, $q)
{
       //     (m1 - m2)
       // k = --------- mod q
       //     (s1 - s2)
    
    return bi_divmod(bi_submod($m1, $m2, $q), bi_submod($s1, $s2, $q), $q);
}


$repeatedR = array_filter(array_count_values($rValues), function($value) {
    return $value > 1;
});
$reusedK = [];
foreach ($rValues as $key => $rValue) {
    if (isset($repeatedR[$rValue])) {
        $reusedK[$rValue][] = $key;
    }
}

$dsa = new DSA($p, $q, $g);
foreach ($reusedK as $keys) {
    $a = $keys[0];
    $b = $keys[1];
    $k = compareSignatures($mValues[$a], $mValues[$b], $sValues[$a], $sValues[$b], $q);
    $x = $dsa->recoverPrivateKey($k, $rValues[$a], $sValues[$a], $msgs[$a], $q);
    $hex = bi_to_str($x, 16);
    echo 'I make private key to be '.$hex.' which hashes to ',hash('sha1', $hex),PHP_EOL;
}

