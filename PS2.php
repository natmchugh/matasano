<?php
namespace Matasano;

require 'vendor/autoload.php';


function ƒ($x) {
    echo  'k[0]';
    for ($i = 1; $i < 5; $i++) {
        if ($x[($i -1)] == 1) {
            echo " xor k[$i]";
        }
    }
    echo PHP_EOL;
}

$x = array(0, 1, 0 ,1);
ƒ($x);

$x = array(0, 1, 1 ,0);
ƒ($x);

$x = array(1, 1, 1 ,0);
ƒ($x);

$x = array(1, 1, 0 ,1);
ƒ($x);

function zerosxorhalfones($zeros, $halfones){
    $zeros = new Message($zeros, Message::HEX);
    $halfones = new Message($halfones, Message::HEX);
    $encoder = new XorEncoder($zeros, $halfones);
    var_dump($encoder->encode());
}

$allzero1 = "5f67abaf5210722b";
$halfones1 = "bbe033c00bc9330e";

zerosxorhalfones($allzero1, $halfones1);

$allzero2 = "9f970f4e932330e4";
$halfones2 = "6068f0b1b645c008";

zerosxorhalfones($allzero2, $halfones2);

$allzero3 = "7c2822ebfdc48bfb";
$halfones3 = "325032a9c5e2364b";

zerosxorhalfones($allzero3, $halfones3);

$allzero4 = "9d1a4f78cb28d863";
$halfones4 = "75e5e3ea773ec3e6";

zerosxorhalfones($allzero4, $halfones4);