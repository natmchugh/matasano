<?php

function DiffieHellman($p, $g) {
    $a = bi_mod(mt_rand(), $p);
    $A = bi_powmod($g, $a, $p);

    $b = bi_mod(mt_rand(), $p);
    $B = bi_powmod($g, $b, $p);

    $s = bi_powmod($B, $a, $p);
    var_dump(bi_to_str($s));
    $s1 = bi_powmod($A, $b, $p);
    return  sha1(bi_to_str($s));
}

$p = bi_base_convert('ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63'.
'b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b57'.
'6625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286'.
'651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c6'.
'2f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff', 16, 10);
$g = 1;
$key = DiffieHellman($p, $g);
var_dump($key);