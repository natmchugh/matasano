<?php

require(__DIR__.'/SHA1.php');

function hmac($key, $message)
{
    $blocksize = 64;
    if (strlen($key) > $blocksize) {
        $key = hash_sha1($key, true); // keys longer than blocksize are shortened
    }
     // keys shorter than blocksize are zero-padded (where ∥ is concatenation)
    while ($blocksize > strlen($key)) {
        $key .= chr(0);
    }
    $o_key_pad = $i_key_pad = '';
    foreach (str_split($key) as $keyChr) {
        $o_key_pad .= chr(0x5c ^ ord($keyChr)); // Where blocksize is that of the underlying hash function
        $i_key_pad .= chr(0x36 ^ ord($keyChr)); // Where ⊕ is exclusive or (XOR)
    }
    return sha1($o_key_pad . sha1($i_key_pad . $message, true)); // Where ∥ is concatenation
}

function callService($hmac, $data)
{
    $url = sprintf('http://localhost:8000/?file=%s&signature=%s', urlencode($data), urlencode($hmac));
    $startTime = microtime(true);
    $response =  @file_get_contents($url);
    $time = microtime(true) - $startTime;
    preg_match('#HTTP/\d+\.\d+ (\d+)#', $http_response_header[0], $matches);
    if (200 == (int) $matches[1]) {
        return 99;
    }
    return $time;
}


$dataString = 'Anything less than the best is a felony';
echo hmac('YELLOW SUBMARINE', $dataString), PHP_EOL;
$possibleChrs = array_merge(range(0x30, 0x39), range(0x61, 0x66));
$hmacToTry = str_repeat('X', 40);
$noTimesToAverage = 3;
$totalTime = 0;
for($j = 0; $j < $noTimesToAverage; $j++) {
    $time = callService($hmacToTry, $dataString);
    $totalTime += $time;
}
$longestTimes = [$totalTime / $noTimesToAverage];
// roughly 1 sigma
$expectedTimeStep = 0.0035;
$roundsSinceFound = 0;

for ($i = 0; $i < 40; $i++) {
    $roundsSinceFound++;
    foreach ($possibleChrs as $possibleChr) {
        $hmacToTry = substr_replace($hmacToTry, chr($possibleChr), $i, 1);
        $totalTime = 0;
        for($j = 0; $j < $noTimesToAverage; $j++) {
            $totalTime += callService($hmacToTry, $dataString);
        }
        $time = $totalTime / $noTimesToAverage;
        if ($time > ($longestTimes[$i] + $expectedTimeStep)) {
            $longestTimes[] = $time;
            echo $hmacToTry, PHP_EOL;
            $roundsSinceFound = 0;
            break 1;
        }
   
    }
    //simple back track if we didn't find a definitive answer go back
    if ($roundsSinceFound !== 0) {
        echo "Try $roundsSinceFound round(s) again",PHP_EOL;
        // longer since found a round more chrs of hmac tried again
        // more importantely increase average
        $noTimesToAverage += 10;
        $i -= $roundsSinceFound;
    }
}

/**
 * Fast implementation which breaks when finding significant increase in time
 * and averages over only a few calls. This method is actually good to 5ms as 
 * in below example
 * * 
 * Example Output:
 * d369b6b8077c21ec919ed7ffd4052d22900a0c3c
 * dXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 * d3XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 * d36XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 * d369XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 * d369bXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 * d369b6XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 * d369b6bXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 * d369b6b8XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 * d369b6b80XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 * d369b6b807XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 * d369b6b8077XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 * d369b6b8077cXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 * d369b6b8077c2XXXXXXXXXXXXXXXXXXXXXXXXXXX
 * d369b6b8077c21XXXXXXXXXXXXXXXXXXXXXXXXXX
 * d369b6b8077c21eXXXXXXXXXXXXXXXXXXXXXXXXX
 * Try 1 round(s) again
 * d369b6b8077c21ecXXXXXXXXXXXXXXXXXXXXXXXX
 * d369b6b8077c21ec9XXXXXXXXXXXXXXXXXXXXXXX
 * d369b6b8077c21ec91XXXXXXXXXXXXXXXXXXXXXX
 * d369b6b8077c21ec919XXXXXXXXXXXXXXXXXXXXX
 * d369b6b8077c21ec919eXXXXXXXXXXXXXXXXXXXX
 * d369b6b8077c21ec919edXXXXXXXXXXXXXXXXXXX
 * d369b6b8077c21ec919ed7XXXXXXXXXXXXXXXXXX
 * d369b6b8077c21ec919ed7fXXXXXXXXXXXXXXXXX
 * d369b6b8077c21ec919ed7ffXXXXXXXXXXXXXXXX
 * d369b6b8077c21ec919ed7ffdXXXXXXXXXXXXXXX
 * d369b6b8077c21ec919ed7ffd4XXXXXXXXXXXXXX
 * d369b6b8077c21ec919ed7ffd40XXXXXXXXXXXXX
 * Try 1 round(s) again
 * d369b6b8077c21ec919ed7ffd405XXXXXXXXXXXX
 * d369b6b8077c21ec919ed7ffd4052XXXXXXXXXXX
 * d369b6b8077c21ec919ed7ffd4052dXXXXXXXXXX
 * d369b6b8077c21ec919ed7ffd4052d2XXXXXXXXX
 * d369b6b8077c21ec919ed7ffd4052d22XXXXXXXX
 * d369b6b8077c21ec919ed7ffd4052d229XXXXXXX
 * d369b6b8077c21ec919ed7ffd4052d2290XXXXXX
 * d369b6b8077c21ec919ed7ffd4052d22900XXXXX
 * d369b6b8077c21ec919ed7ffd4052d22900aXXXX
 * d369b6b8077c21ec919ed7ffd4052d22900a0XXX
 * d369b6b8077c21ec919ed7ffd4052d22900a0cXX
 * d369b6b8077c21ec919ed7ffd4052d22900a0c3X
 * d369b6b8077c21ec919ed7ffd4052d22900a0c3c
 */
 
