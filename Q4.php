<?php

namespace Matasano;

require 'vendor/autoload.php';

$url = 'https://gist.github.com/tqbf/3132713/raw/40da378d42026a0731ee1cd0b2bd50f66aabac5b/gistfile1.txt';

$handle = fopen($url, 'r');
$line = 0;
while (($buffer = fgets($handle, 4096)) !== false) {
    $line++;
    $message = new Message(trim($buffer), Message::HEX);
    $xorEncoder = new XorEncoder($message);
    $key = $xorEncoder->getBestKey();
    if (!empty($key)) {
        $xorEncoder->setKey($key);
        $decoded = $xorEncoder->decode();
        echo "Line number $line",PHP_EOL;
        echo "Key is $key",PHP_EOL;
        echo $decoded,PHP_EOL;
    }
}

