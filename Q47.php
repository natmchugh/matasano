<?php

namespace Matasano;

require 'vendor/autoload.php';

$k = 384;
$rsa = new RSAOracle($k);

list($e, $n) = $rsa->getPublicKey();

$message = 'kick it, CC';
$c = $rsa->encrypt($message);

$power = 8 * ($k / 8 - 2);
$B = bi_pow(2, $power);
$_2B = bi_mul(2, $B);
$_3B = bi_mul(3, $B);
$s1 = bi_ceil_div($n , $_3B);
$m0 = (new Message($c))->toDecimal();

var_dump('n', bi_to_str($n, 16));
var_dump('e', bi_to_str($e));

$si = $s1;
$i = $it = 1;
$M = [[0 => [$_2B, $_3B]]];

do {
    
    if (count($M[$i - 1]) > 1 || $i == 1) {
        if (1 === $i) {
            echo '[Step 2a] Starting the search.',PHP_EOL;
        } else {
            echo '[Step 2b] Searching with more than one interval left.',PHP_EOL;
        }
        echo "Starting search s_$i from $si",PHP_EOL;
        while(true) {
            $m1 = bi_mulmod(bi_powmod($si, $e, $n), $m0, $n);
            $encrypted = new Message(bi_to_str($m1), Message::DECIMAL);
            if ($rsa->oracle($encrypted)) {
                break 1;
            }
            ++$it;
            ++$si;
        }
        echo "Found s_$i in $it interations s_$i = ",$si,PHP_EOL;
        echo "Now doing the intersections of s_$i with [a, b] ",PHP_EOL;

        foreach ($M[$i - 1] as list($a, $b)) {
            //(2Bs1 - 3B + 1) / n
            $rMin = bi_div(bi_sub(bi_mul($a, $si), bi_add($_3B, 1)), $n);
            //((3B-1)s1 - 2B) / n
            $rMax = bi_div(bi_sub(bi_mul($b, $si), $_2B), $n);
            $rValues = range((int) bi_to_str($rMin), (int) bi_to_str($rMax));

            echo 'Found ',count($rValues),' possible r values',PHP_EOL;
                
            foreach ($rValues as $k => $r) {
                echo 'Trying r of ',$r,PHP_EOL;
                $lowerBound = bi_ceil_div(bi_add($_2B, bi_mul($r, $n)), $si);
                $upperBound = bi_floor_div(bi_add(bi_sub($_3B, 1), bi_mul($r, $n)), $si);
                //chose either the new bounds or 2B, 3B if they are outside of these
                $_a = bi_max($a, $lowerBound);
                $_b = bi_min(bi_sub($b, 1),$upperBound);
                if (1 > bi_cmp($_a, $_b)) {
                    echo 'Found an interval ',PHP_EOL;
                    echo bi_to_str($_a, 16),PHP_EOL;
                    echo bi_to_str($_b, 16),PHP_EOL;
                    $M[$i][] = [$_a, $_b];
                }
            }
        }
        echo 'There are ',count($M[$i]),' intervals',PHP_EOL;
    } else {
        echo '[Step 2c] Searching with one interval left',PHP_EOL;
        $interval = current($M[$i-1]);
        list($a, $b) = $interval;
        $r = bi_mul(2, bi_div(bi_sub(bi_mul($b, $si), $_2B) , $n));
        $i2c = 0;
        $nr = 1;
        while (true) {
            $minSi = bi_div(bi_add($_2B, bi_mul($r, $n)), $b);
            $maxSi = bi_div(bi_add(bi_sub($_3B, 1), bi_mul($r, $n)), $a);
            $sis = bi_range($minSi, $maxSi);
            foreach ($sis as $si) {
                $mi = bi_mulmod(bi_powmod($si, $e, $n), $m0, $n);
                ++$i2c;
                $message = new Message(bi_to_str($mi,16), Message::HEX);
                if ($rsa->oracle($message)) {
                    break 2;
                }
            }
            $r = bi_add($r, 1);
            ++$nr;
        }
        echo "[*] Search done in $i2c iterations",PHP_EOL;
        echo "    explored values of r:  $nr",PHP_EOL;
        echo "    s_$i:                    ",bi_to_str($si),PHP_EOL;
        echo '[Step 3]  Narrowing the set of solutions.',PHP_EOL;
        $lowerBound = bi_ceil_div(bi_add($_2B, bi_mul($r, $n)), $si);
        $upperBound = bi_floor_div(bi_add(bi_sub($_3B, 1), bi_mul($r, $n)), $si);
        $a = bi_max($a, $lowerBound);
        $b = bi_min($b, $upperBound);
        echo '[Step 4] Computing the solution.',PHP_EOL;
        if (0 === bi_cmp($a, $b)) {
            echo '====DONE====',PHP_EOL;
            break 1;
        }
        echo 'There are still ',bi_to_str(bi_sub($b, $a)),' possible solutions',PHP_EOL;
        $M[$i][] = [$a, $b];
    }
    ++$si;
    ++$i;
} while(true);

echo new Message(bi_to_str($a), Message::DECIMAL),PHP_EOL;
echo "In {$rsa->getCalls()} calls",PHP_EOL;
