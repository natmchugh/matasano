<?php

namespace Matasano;

require 'vendor/autoload.php';


$config = array(
    "digest_alg" => "sha1",
    "private_key_bits" => 1024,
    "private_key_type" => OPENSSL_KEYTYPE_RSA,
);
    
// Create the private and public key
$res = openssl_pkey_new($config);

// Extract the private key from $res to $privKey
openssl_pkey_export($res, $privKey);

// Extract the public key from $res to $pubKey
$pubKey = openssl_pkey_get_details($res);

$e = bi_base_convert(bin2hex($pubKey['rsa']['e']), 16, 10);
$n = bi_base_convert(bin2hex($pubKey['rsa']['n']), 16, 10);

function oracleIsEven($encrypted, $privKey)
{
    openssl_private_decrypt($encrypted, $decrypted, $privKey, OPENSSL_NO_PADDING);
    return lsbIsZero($decrypted);
}

function lsbIsZero($decrypt)
{
    $decrypt = new Message($decrypt);
    return 0 == bi_test_bit($decrypt->toDecimal(), 0);
}

function doubleCypherText($ct, $e, $n)
{
    // ct * (2**e)%n
    return $ct->doubleExponent($e, $n);
}

$data = base64_decode('VGhhdCdzIHdoeSBJIGZvdW5kIHlvdSBkb24ndCBwbGF5IGFyb3VuZCB3aXRoIHRoZSBGdW5reSBDb2xkIE1lZGluYQ');
openssl_public_encrypt($data, $cypherText, $pubKey['key']);

$cypherText = new Message($cypherText);

$lowerBound = bi_from_str('0');
$upperBound = bi_sub($n, 1);
for ($i = 0; $i < 1025; $i++) {
    $cypherText = doubleCypherText($cypherText, $e, $n);
    if (oracleIsEven($cypherText, $privKey)) {
        $upperBound = bi_div(bi_add($upperBound, $lowerBound), 2);
    } else {
        $lowerBound = bi_div(bi_add($upperBound, $lowerBound), 2);
    }
    echo new Message(bi_to_str($lowerBound), Message::DECIMAL),PHP_EOL;
}
