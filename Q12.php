<?php

namespace Matasano;

require 'vendor/autoload.php';

// find block size
$message = new Message();
for ($i = 1; $i < 255; $i++) {
    $input = str_repeat('A', $i);
    $cypherText = $message->ecb_encryption_oracle($input);
    $cyperMessage = new Message($cypherText);
    $analyser = new Analyser($cyperMessage);
    if ($analyser->containsDuplicates()) {
        $blockSize = $i / 2;
        break;
    }
}
$letters = '';

$message = new Message();
$cypherText = $message->ecb_encryption_oracle('');
//will always be multiple of block length
$textLength = strlen($cypherText);

$possibleOrds = range(0, 255);

for ($i = 1; $i <= $textLength; $i++) {
    //create string known intial part making sure chr to decrypt is at end of a block
    $oneByteLess = str_repeat('A', $textLength - $i);
    $cypherText = $message->ecb_encryption_oracle($oneByteLess);
    // get part of cypher text with chr of interest at end of the string
    $chrUnderTest = substr($cypherText, 0 , $textLength);
    $dictionary = array();
    foreach ($possibleOrds as $possibleOrd) {
        $chr = chr($possibleOrd);
        //create a string of our known pdding plus text already decrypted plus new poss chr
        $block = $oneByteLess.$letters.$chr;
        // populate dictionary with decrypted text plus poss new chr
        $dictionary[$letters.$chr] = substr($message->ecb_encryption_oracle($block), 0, $textLength);
    }

    $letters = array_search($chrUnderTest, $dictionary);
    if ($letters) {
        // dramatic pause
        usleep(20000);
        echo substr($letters, -1);
    }
}
echo PHP_EOL;

// output:
// Rollin' in my 5.0
// With my rag-top down so my hair can blow
// The girlies on standby waving just to say hi
// Did you stop? No, I just drove by
