<?php

require(__DIR__.'/SHA1.php');

$file = $_GET['file'];
$signature = $_GET['signature'];

function hmac($key, $message)
{
    $blocksize = 64;
    if (strlen($key) > $blocksize) {
        $key = hash_sha1($key, true); // keys longer than blocksize are shortened
    }
     // keys shorter than blocksize are zero-padded (where ∥ is concatenation)
    while ($blocksize > strlen($key)) {
        $key .= chr(0);
    }
    $o_key_pad = $i_key_pad = '';
    foreach (str_split($key) as $keyChr) {
        $o_key_pad .= chr(0x5c ^ ord($keyChr)); // Where blocksize is that of the underlying hash function
        $i_key_pad .= chr(0x36 ^ ord($keyChr)); // Where ⊕ is exclusive or (XOR)
    }
    return sha1($o_key_pad . sha1($i_key_pad . $message, true)); // Where ∥ is concatenation
}

function insecure_compare($a, $b) {
    foreach (str_split($a) as $i => $chrA) {
        if ($chrA != substr($b, $i, 1)) {
            return false;
        }
        // 5000 * 10 ^-6s = 5ms
        usleep(5000);
    }
    return true;
}

$key = 'YELLOW SUBMARINE';
if (insecure_compare($signature, hmac($key, $file))) {
    die('HMAC is good thanks for the file.');
}

http_response_code(500);
die('Uhuh HMAC looks like crap not taking your file.');