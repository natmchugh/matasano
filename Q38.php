<?php

function bichrdec($chrs)
{
    $dec = 0;
    $len = strlen($chrs);
    for ($i = 1; $i <= $len; $i++) {
        $dec = bi_add($dec, bi_mul((ord($chrs[$i - 1])), bi_pow('16', strval($len - $i))));
    }
    return $dec;
}

// C & S           Agree on N=[NIST Prime], g=2, k=3, I (email), P (password)

$N = bi_base_convert('ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63'.
'b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b57'.
'6625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286'.
'651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c6'.
'2f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff', 16,10);
$g = 2;
$k =3;
$password = 'toocold';

function Server($sockets) {
    global $N, $g, $k, $password;
//Server
    $salt = openssl_random_pseudo_bytes(16);
    $xH = hash('sha256', $salt.$password);
    $x = bi_to_str(bi_base_convert($xH, 16, 10));

    // S               x = SHA256(salt|password)
    //                 v = g**x % n
    $v = bi_powmod($g, $x, $N);
    $fromClient = socket_read($sockets[0], 1024, PHP_BINARY_READ);
    list($I, $A) = json_decode($fromClient);
    // S->C            salt, B = g**b % n, u = 128 bit random number
    $b = bi_mod(mt_rand(), $N);
    $B = bi_add(bi_mul($k, $v) , bi_powmod($g, $b, $N));
    $uB = openssl_random_pseudo_bytes(16);
    $u = bi_to_str(bichrdec($uB));
    
    $toClient = json_encode([base64_encode($salt), bi_to_str($B), $u]);
    socket_write($sockets[0], $toClient, strlen($toClient));

    // S           S = (A * v ** u)**b % n
    //             K = SHA256(S)
    $S = bi_powmod(bi_mul($A, bi_powmod($v, $u, $N)), $b, $N);
    $K = hash('sha256', bi_to_str($S));
    $hmac = socket_read($sockets[0], 1024, PHP_BINARY_READ);
    echo $hmac == hash_hmac('sha256', $salt, $K) ? 'OK' : 'BAD', PHP_EOL;
}

function Client($sockets) 
{
    global $N, $g, $k, $password;
    $I = 'vanilla@iceice.baby';
    //Client
    $a = bi_mod(mt_rand(), $N);
    $A = bi_powmod($g, $a, $N);
    // C->S            I, A = g**a % n
    $toServer = json_encode([$I, bi_to_str($A)]).PHP_EOL;
    socket_write($sockets[1], $toServer, strlen($toServer));

    $fromServer = socket_read($sockets[1], 1024, PHP_BINARY_READ);
    list($salt, $B, $u) = json_decode($fromServer);
    $salt = base64_decode($salt);
    // C            x = SHA256(salt|password)
    //              S = B**(a + ux) % n
    //              K = SHA256(S)
    $xH = hash('sha256', $salt.$password);
    $x = bi_to_str(bi_base_convert($xH, 16, 10));
    $S = bi_powmod(bi_sub($B, bi_mul($k, bi_powmod($g, $x, $N))), bi_add($a, bi_mul($u, $x)), $N);
    $K = hash('sha256', bi_to_str($S));

    $hmac = hash_hmac('sha256', $salt, $K);
    socket_write($sockets[1], $hmac, strlen($hmac));
    sleep(2);
}

function Attacker($sockets)
{
    list($serverPair, $clientPair) = $sockets;

    $fromClient = socket_read($clientPair[0], 1024, PHP_BINARY_READ);
    socket_write($serverPair[1], $fromClient, strlen($fromClient));

    $fromServer = socket_read($serverPair[1], 1024, PHP_BINARY_READ);
    socket_write($clientPair[0], $fromServer, strlen($fromServer));

    $fromClient = socket_read($clientPair[0], 1024, PHP_BINARY_READ);
    socket_write($serverPair[1], $fromClient, strlen($fromClient));


}

$children = $sockets = $pair = array();
socket_create_pair(AF_UNIX, SOCK_STREAM, 0, $pair);
$sockets[] = $pair;
socket_create_pair(AF_UNIX, SOCK_STREAM, 0, $pair);
$sockets[] = $pair;
for ($i = 0; $i < 3; $i++) {
    $children[] = $pid = pcntl_fork();
    if (0 === $pid) {
        echo 'Starting ';
        switch($i) {
            case 0:
                echo 'Server'.PHP_EOL;
                Server($sockets[0]);
                die();
            break;
            case 1:
                echo 'Client'.PHP_EOL;
                Client($sockets[1]);
                die();
            break;
            case 2:
                echo 'Attacker'.PHP_EOL;
                Attacker($sockets);
                die();
            break;
        }
    }
}

do {
    $pid = pcntl_wait($status);
    $children = array_diff($children, array($pid));
} while(count($children) > 0);
