<?php

namespace Matasano;


function bigRandomNumber() {
    $bytes = openssl_random_pseudo_bytes(64);
    $dec = 0;
    foreach (str_split($bytes) as $i => $chr) {
        $dec = bi_add($dec, bi_mul(ord($chr), bi_pow('100', $i+1)));
    }
    return $dec;
}

function encrypt($plainText) {
    $message = new Message($plainText);
    do {
        // - Generate 2 random primes. We'll use small numbers to start, so you
        //  can just pick them out of a prime table. Call them "p" and "q".
        $rand = bigRandomNumber();
        $p = bi_next_prime($rand);
        $rand = bigRandomNumber();
        $q = bi_next_prime($rand);
        // - Let n be p * q. Your RSA math is modulo n.
        $n = bi_mul($p, $q); 

        // 
        // - Let et be (p-1)*(q-1) (the "totient"). You need this value only for
        //  keygen.

        $et = bi_mulmod(bi_sub($p, 1) , bi_sub($q, 1), $n);

        // - Let e be 3.

        $e = 3;

        $coPrime = true;
        try {
            // - Compute d = invmod(e, et). invmod(17, 3120) is 2753.
            $d = NumberTheory::invMod($e, $et);
        } catch (\OutOfBoundsException $exception) {
            $coPrime = false;
        }
    } while (!$coPrime);

    // Your public key is [e, n]. Your private key is [d, n].


    // To encrypt: c = m**e%n. To decrypt: m = c**d%n
    $c = bi_powmod($message->toDecimal(), $e, $n);
    return [$c, $e, $n];
}

