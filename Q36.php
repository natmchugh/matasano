<?php

// C & S           Agree on N=[NIST Prime], g=2, k=3, I (email), P (password)

$N = bi_base_convert('ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63'.
'b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b57'.
'6625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286'.
'651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c6'.
'2f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff', 16, 10);
$g = 2;
$k =3;
$I = 'vanilla@iceice.baby';
$password = "you'll never guess";

$sockets = array();
socket_create_pair(AF_UNIX, SOCK_STREAM, 0, $sockets);
$pid = pcntl_fork();

if ($pid) {
//Server
    while (true) {
    $salt = openssl_random_pseudo_bytes(16);
    // S               1. Generate salt as random integer
    //                 2. Generate string xH=SHA256(salt|password)
    $xH = hash('sha256', $salt.$password);
    //                 3. Convert xH to integer x somehow (put 0x on hexdigest)
    $x = bi_to_str(bi_base_convert($xH, 16, 10));
    //                 4. Generate v=g**x % N
    $v = bi_powmod($g, $x, $N);
    //                 5. Save everything but x, xH
    $fromClient = socket_read($sockets[0], 1024, PHP_BINARY_READ);
    list($I, $A) = json_decode($fromClient);

    $b = bi_mod(mt_rand(), $N);
    $B = bi_add(bi_mul($k, $v) , bi_powmod($g, $b, $N));
    // S->C            Send salt, B=kv + g**b % N
    $toClient = json_encode([base64_encode($salt), bi_to_str($B)]);
    socket_write($sockets[0], $toClient, strlen($toClient));

    // S, C            Compute string uH = SHA256(A|B), u = integer of uH
    $uH = hash('sha256', $A.bi_to_str($B));
    $u = bi_to_str(bi_base_convert($uH, 16, 10));
    // S               1. Generate S = (A * v**u) ** b % N
    $S = bi_powmod(bi_mul($A, bi_powmod($v, $u, $N)), $b, $N);
    //                 2. Generate K = SHA256(S)
    $K = hash('sha256', bi_to_str($S));
    $hmac = socket_read($sockets[0], 1024, PHP_BINARY_READ);
    // S->C            Send "OK" if HMAC-SHA256(K, salt) validates
    echo $hmac == hash_hmac('sha256', $salt, $K) ? 'OK' : 'BAD', PHP_EOL;
    }
} else {
    //Client
    $a = bi_mod(mt_rand(), $N);
    $A = bi_powmod($g, $a, $N);
    // C->S            Send I, A=g**a % N (a la Diffie Hellman)

    while (true) {
        $handle = fopen ("php://stdin","r");
        echo 'Email: ';
        $I = trim(fgets($handle));
        echo 'Password: ';
        $password = trim(fgets($handle));

        $toServer = json_encode([$I, bi_to_str($A)]).PHP_EOL;
        socket_write($sockets[1], $toServer, strlen($toServer));

        $fromServer = socket_read($sockets[1], 1024, PHP_BINARY_READ);
        list($salt, $B) = json_decode($fromServer);
        $salt = base64_decode($salt);
        // S, C            Compute string uH = SHA256(A|B), u = integer of uH
        $uH = hash('sha256', bi_to_str($A).$B);
        $u = bi_base_convert($uH, 16, 10);
        // C               1. Generate string xH=SHA256(salt|password)
        $xH = hash('sha256', $salt.$password);
        //                 2. Convert xH to integer x somehow (put 0x on hexdigest)
        $x = bi_to_str(bi_base_convert($xH, 16, 10));
        //                 3. Generate S = (B - k * g**x)**(a + u * x) % N
        $S = bi_powmod(bi_sub($B, bi_mul($k, bi_powmod($g, $x, $N))), bi_add($a, bi_mul($u, $x)), $N);
        //                 4. Generate K = SHA256(S)
        $K = hash('sha256', bi_to_str($S));
        // C->S            Send HMAC-SHA256(K, salt)
        $hmac = hash_hmac('sha256', $salt, $K);
        socket_write($sockets[1], $hmac, strlen($hmac));
        sleep(2);
    }
}
