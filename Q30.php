<?php

namespace Matasano;

require 'vendor/autoload.php';

$secretKey = (new Message)->genKey(mt_rand(1, 99));

function isValid($message, $hash, $secretKey) {
    $md4 = new MD4($secretKey.$message);
    return $hash == $md4->hash();
}


function computePadding($message, $keyLength) {
    $originalSizeBits = (strlen($message) + $keyLength) * 8;
    $message .= chr(128);
    while ((((strlen($message) + $keyLength) + 8) % 64) !== 0) {
        $message .= chr(0);
    }
    $masks = [0x00000000ffffffff, 0xffffffff00000000];
    foreach ($masks as $mask) {
        $ords = unpack('C*' , pack('L', $originalSizeBits & $mask));
        foreach ($ords as $ord) {
            $message .= chr($ord);
        }
    }
    return substr($message, $originalSizeBits / 8 - $keyLength);
}

$originalMessage = 'comment1=cooking%20MCs;userdata=foo;comment2=%20like%20a%20pound%20of%20bacon';
$md4 = new MD4($secretKey.$originalMessage);
$extension = ';admin=true';
$originalHash = $md4->hash();
$registers = unpack('V4', hex2bin($originalHash));
for ($i = 1; $i < 100; $i++) {
    $gluePadding = computePadding($originalMessage, $i);
    $md4 = new MD4('');
    $finalPadding = computePadding($originalMessage.$gluePadding.$extension, $i);
    $md4->setMessage($extension.$finalPadding);
    $md4->setInitalRegisters(array_values($registers));
    $hash = $md4->hash();
    $valid = isValid($originalMessage.$gluePadding.$extension, $hash, $secretKey);
    if ($valid) {
        echo 'Found valid hash: ',$hash,PHP_EOL;
        echo 'Secret key length is: ',$i,PHP_EOL;
        break;
    }
}

/**
 *  There were no good MD4 implementations in PHP so had to write this which took a
 *  long time. 
 *
 * Example Output:
 * Found valid hash: be6b4a506c4e2bdf7f6a9a9dd614f1c5
 * Secret key length is: 91
 * 
 */
