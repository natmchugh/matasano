<?php

namespace Matasano;

require 'vendor/autoload.php';

$message = new Message();
$randomKey = $message->genKey(16);

$userString = 'email=AAAAAAAAAAadmin'.str_repeat(chr(11), 11).'&uid=10&role=user';
$encryptedUser = openssl_encrypt($userString, 'AES-128-ECB', $randomKey, OPENSSL_RAW_DATA);

function decryptAndParse($encodeProfile, $key)
{
    $profileString = openssl_decrypt($encodeProfile, 'AES-128-ECB', $key, OPENSSL_RAW_DATA);
    $mesage = new Message($profileString);
    return $mesage->parseKeyValues($profileString);
}

// pad with junk to make sure word 'user' is at begining of a block
$targetString = 'email=fooAA@bar.com&uid=10&role=user';

$encryptedAdmin = openssl_encrypt($targetString, 'AES-128-ECB', $randomKey, OPENSSL_RAW_DATA);

//split the encrpyted text into blocks
$encryptedAdminBlocks = str_split($encryptedAdmin, 16);
$encryptedUserBlocks = str_split($encryptedUser, 16);

// swap out last block for padded word admin
$encryptedAdminBlocks[2] = $encryptedUserBlocks[1];


$encryptedAdmin = implode($encryptedAdminBlocks);
var_dump(decryptAndParse($encryptedAdmin, $randomKey));

/*
Output:

array(3) {
  ["email"]=>
  string(13) "fooAA@bar.com"
  ["uid"]=>
  string(2) "10"
  ["role"]=>
  string(5) "admin"
}
 */