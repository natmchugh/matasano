<?php

namespace Matasano;

require 'vendor/autoload.php';               

$cypherText = "L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ==";

$cypherText = new Message($cypherText, Message::BASE64);

$key = 'YELLOW SUBMARINE';
$nonce = str_repeat(chr(0), 8);

echo $cypherText->ctr_decrypt($key, $nonce, Message::NONCE),PHP_EOL;

// Output is:
// Yo, VIP Let's kick it Ice, Ice, baby Ice, Ice, baby