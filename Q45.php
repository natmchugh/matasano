<?php

namespace Matasano;

require 'vendor/autoload.php';

$p = bi_base_convert('800000000000000089e1855218a0e7dac38136ffafa72eda7'.
 '859f2171e25e65eac698c1702578b07dc2a1076da241c76c6'.
 '2d374d8389ea5aeffd3226a0530cc565f3bf6b50929139ebe'.
 'ac04f48c3c84afb796d61e5a4f9a8fda812ab59494232c7d2'.
 'b4deb50aa18ee9e132bfa85ac4374d7f9091abc3d015efc87'.
 '1a584471bb1', 16 ,10);

$q = bi_base_convert('f4f47f05794b256174bba6e9b396a7707e563c5b', 16, 10);

$g = bi_base_convert('5958c9d3898b224b12672c0b98e06c60df923cb8bc999d119'.
 '458fef538b8fa4046c8db53039db620c094c9fa077ef389b5'.
 '322a559946a71903f990f1f7e0e025e2d7f7cf494aff1a047'.
 '0f5b64c36b625a097f1651fe775323556fe00b3608c887892'.
 '878480e99041be601a62166ca6894bdd41a7054ec89f756ba'.
 '9fc95302291', 16, 10);

$message = "Hello, world";

$dsa = new DSA($p, $q, 0);
$dsa->genKeys();

$signature = $dsa->sign($message);

list($r, $s) = $signature;

echo "r is zero uhoh: ",bi_to_str($r),PHP_EOL;
$veracity = $dsa->verify($message, $r, $s);
echo 'Does it verify? ',$veracity?'yes':'no',PHP_EOL;
$veracity = $dsa->verify('any other string', $r, $s);
echo 'Trying random string? ',$veracity?'yes':'no',PHP_EOL;

function getMagicSignature($message, $y, $p, $q)
{
    $z = bi_base_convert(hash('sha1', $message), 16, 10);
    // r = ((y**z) % p) % q
    $r = bi_mod(bi_powmod($y, $z, $p), $q);
    //       r
    // s =  --- % q
    //       z
    $s = bi_divmod($r, $z, $q);
    return [$r, $s];
}

$dsa = new DSA($p, $q, bi_add($p, 1));
$dsa->genKeys();
list($p, $q, $g, $y) = $dsa->getPublicKey();
$signature = getMagicSignature($message, $y, $p, $q);
list($r, $s) = $signature;
$veracity = $dsa->verify($message, $r, $s);
echo 'Does it verify? ',$veracity?'yes':'no',PHP_EOL;

var_dump(bi_to_str($r), bi_to_str($s));
$signature = getMagicSignature($message, $y, $p, $q);
list($r, $s) = $signature;
var_dump(bi_to_str($r), bi_to_str($s));
$veracity = $dsa->verify('Goodbye, world', 1, 1);
echo 'Does it verify? ',$veracity?'yes':'no',PHP_EOL;
