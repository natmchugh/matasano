<?php

namespace Matasano;

require 'vendor/autoload.php';

function isValid($message, $hash)
{
    $secretKey = 'ds8sa9dg89gsav8sv8';
    $message = new Message($secretKey.$message);
    $sha = new SHA1($message);
    return $hash == $sha->hash();
}

$hash = '1233e9b362c4c8e9a48cb6ef565fb1c7d953d61e';

var_dump(isValid('sfaf', $hash));
var_dump(isValid('You have just been poisioned', '1233e9b362c4c8e9a48cb6ef565fb1c7d953d61f'));
var_dump(isValid('You have just been poisioned', $hash));


/*
 * Example Output:\
 *
 * There were no good SHA1 implementations in pure PHP so had to write
 * 
 * bool(false)
 * bool(false)
 * bool(true)
 * 
 */