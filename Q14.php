<?php

namespace Matasano;

require 'vendor/autoload.php';
$message = new Message();
$prefix = $message->genKey(mt_rand(1, 50));

$blockSize = 16;
$message = new Message();
$cypherText = $message->ecb_encryption_oracle_prefix('', $prefix);
//will always be multiple of block length
$textLength = strlen($cypherText);

$possibleOrds = range(0, 255);
$letters = '';

$prefixLength = 0;

// find prefix length by adding at least 32 bytes the same and looking for duplicate ciphertext blocks
for ($i = 16; $i < $textLength; $i++) {
    $testString = str_repeat('A', 16 + $i);
    $encrypedMessage = $message->ecb_encryption_oracle_prefix($testString, $prefix);
    $blocks = str_split($encrypedMessage, 16);

    $lastBlock = '';

    foreach ($blocks as $block) {
        if ($block == $lastBlock) {
            // look for first of our repeated blocks less the extra bytes to pad out a block
            $prefixLength = strpos($encrypedMessage, $block) - ($i % 16);
            break 2;
        }
        $lastBlock = $block;
    }
}

// same as Q12 but start after prefix
for ($i = $prefixLength +1 ; $i < $textLength; $i++) {

    //create string known intial part making sure chr to decrypt is at end of a block
    $oneByteLess = str_repeat('A', $textLength - $i);
    $cypherText = $message->ecb_encryption_oracle_prefix($oneByteLess, $prefix);
    // get part of cypher text with chr of interest at end of the string

    $chrUnderTest = substr($cypherText, 0 , $textLength);
    $dictionary = array();
    foreach ($possibleOrds as $possibleOrd) {
        $chr = chr($possibleOrd);
        //create a string of our known padding plus text already decrypted plus new poss chr
        $block = $oneByteLess.$letters.$chr;
        // populate dictionary with decrypted text plus poss new chr
        $dictionary[$letters.$chr] = substr($message->ecb_encryption_oracle_prefix($block, $prefix), 0, $textLength);
    }

    $letters = array_search($chrUnderTest, $dictionary);
    if ($letters) {
        // dramatic pause
        usleep(10000);
        echo substr($letters, -1);
    }
}
echo PHP_EOL;

/*
output is

Rollin' in my 5.0
With my rag-top down so my hair can blow
The girlies on standby waving just to say hi
Did you stop? No, I just drove by
*/