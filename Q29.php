<?php

namespace Matasano;

require 'vendor/autoload.php';

$secretKey = (new Message)->genKey(mt_rand(1, 99));

function isValid($message, $hash, $secretKey) {
    $message = new Message($secretKey.$message);
    $sha = new SHA1($message);
    return $hash == $sha->hash();
}

function computePadding($message, $keyLength) {
    $originalSize = strlen($message);
    $message .= chr(128);
    while ((((strlen($message)+$keyLength) + 8) % 64) !== 0) {
        $message .= chr(0);
    }
    foreach (str_split(sprintf('%064b', ($originalSize+$keyLength)*8), 8) as $bin) {
        $message .= chr(bindec($bin));
    }
    return substr($message, $originalSize);
}

$originalMessage = 'comment1=cooking%20MCs;userdata=foo;comment2=%20like%20a%20pound%20of%20bacon';
$originalHash = sha1($secretKey.$originalMessage);
$registers = str_split($originalHash, 8);
foreach ($registers as $i => $register) {
    $registers[$i] = hexdec($register);
}

for ($i = 1; $i < 100; $i++) {
    $gluePadding = computePadding($originalMessage, $i);
    $extension = ';admin=true';
    $sha = new SHA1(new Message());
    $finalPadding = computePadding($originalMessage.$gluePadding.$extension, $i);
    $message = new Message($extension.$finalPadding);
    $sha->setMessage($message);
    $sha->setInitalRegisters($registers);
    $hash = $sha->hash();

    $valid = isValid($originalMessage.$gluePadding.$extension, $hash, $secretKey);
    if ($valid) {
        echo 'Found valid hash: ',$hash,PHP_EOL;
        echo 'Secret key length is: ',$i,PHP_EOL;
        break;
    }
}

/**
 * Really nice attack. Have made the mistake of thinking this is secure before
 * 
 * Example Output:
 * Found valid hash: a48d9f2e0660f293b4a0df4b3a757d334de8a8e0
 * Secret key length is: 47
 * 
 */
