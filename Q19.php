<?php

namespace Matasano;

require 'vendor/autoload.php';

$plainTexts = file(__DIR__.'/q19_texts.txt');
$cypherTexts = array();

// create on esecret key
$key = (new Message)->genKey(16);
// use same nonce for all encryptions
$nonce = (new Message)->genKey(8);

foreach ($plainTexts as $plainText) {
    $plainText = new Message($plainText, Message::BASE64);
    // repeated encryptions using same nonce
    $cypherTexts[] =  $plainText->ctr_encrypt($key, $nonce, Message::NONCE);
}

$keyStream = array();

$ords = range(0, 0xff);
$asciiLetters = array_merge(array(32), range(65, 90), range(97, 122));
// try all ords and see if they give an ascii letter if so score it
foreach ($cypherTexts as $cypherText) {
    foreach ($cypherText as $i => $Cchr) {
        foreach ($ords as $posKeyChr) {
            $posPlainChr = $posKeyChr ^ $Cchr;
            if (in_array($posPlainChr, $asciiLetters)) {
                if (isset($keyStream[$i][$posKeyChr])) {
                    $keyStream[$i][$posKeyChr]++;
                } else {
                    $keyStream[$i][$posKeyChr] = 1;
                }
            }   
        }
    }
}
// find the ord that gave highest score for number of ascii letters
$keyStream = array_map(function($possKeyChrs) {
    $highest = array(0,0);
    foreach ($possKeyChrs as $possKeyChr => $score) {
        if ($score > $highest[0]) {
            $highest = array($score, $possKeyChr);
        }
    }
    return $highest[1];
}, $keyStream);
// use highest scoring ord as the keystream
foreach ($cypherTexts as $cypherText) {
    foreach ($cypherText as $i => $ord) {
        echo chr($keyStream[$i] ^ $ord);
    }
    echo PHP_EOL;
}

/*
    Might not be 'Carmen Sandiego' style but was first algo. to come to mind

 Example Output:
I have met them at close of daY
Coming with vivid faces
From counter or desk among greY
Eighteenth-century houses.
I have passed with a nod of thE hiUN
Or polite meaningless words,
Or have lingered awhile and saId
Polite meaningless words,
And thought before I had done
Of a mocking tale or a gibe
To please a companion
Around the fire at the club,
Being certain that they and I
But lived where motley is worn
All changed, changed utterly:
A terrible beauty is born.
That woman's days were spent
In ignorant good will,
Her nights in argument
Until her voice grew shrill.
What voice more sweet than herS
When young and beautiful,
She rode to harriers?
This man had kept a school
And rode our winged horse.
This other his helper and frieNd
Was coming into his force;
He might have won fame in the End 
So sensitive his nature seemed

So daring and sweet his thoughT.
This other man I had dreamed
A drunken, vain-glorious lout.
He had done most bitter wrong
To some who are near my heart,
Yet I number him in the song;
He, too, has resigned his part
In the casual comedy;
He, too, has been changed in hIs xAX S
Transformed utterly:
A terrible beauty is born.
*/ 
*/
