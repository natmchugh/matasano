<?php

namespace Matasano;

require 'vendor/autoload.php';

$message = new Message();

$key = $message->genKey(16);

echo "You are looking for the key: ".base64_encode($key), PHP_EOL;

function encrypt($message, $key)
{
    $iv = $key;
    $message = new Message($message);
    return $message->cbc_encrypt($key, $iv);
}

function decrypt(Message $cypherText, $key)
{
    // bad thing to do
    $iv = $key;
    $plainText = $cypherText->cbc_decrypt($key, $iv);
    foreach ($plainText as $ord) {
        if ($ord > 127) {
            $errorMessage = 'Your Message got some weird foreigner looking letters: ';
            $errorMessage .= $plainText;
            throw new \UnexpectedValueException ($errorMessage);
        }
    }

    return $plainText;
}

$threeBlockMessage = str_repeat('A', 3 * 16);

$message = new Message($threeBlockMessage);

$encrypted = encrypt($message, $key);

$block = str_split($encrypted, 16);

$encrypted = $block[0].str_repeat(chr(0), 16).$block[0];

try {
    decrypt(new Message($encrypted), $key);
} catch(\UnexpectedValueException $e) {
    $fullError = $e->getMessage();
    $textToIgnore = 'Your Message got some weird foreigner looking letters: ';
    $plainText = substr($fullError, strlen($textToIgnore), 48);
    $p0 = new Message(substr($plainText, 0, 16));
    $p3 = new Message(substr($plainText, 32, 16));
    $xor = new XorEncoder($p0, $p3);
    $key = $xor->encode();
    echo 'Found key: '.$key->toBase64(), PHP_EOL;
}


/**
 * Just followed the steps on this one, guess a longer could be obtained by using
 *  more blocks
 * 
 * Example Output:
 * * 
 * You are looking for the key: 5lkGRmGVIy9nZfGmDki1RA==
 * Found key: 5lkGRmGVIy9nZfGmDki1RA==
 * 
 */