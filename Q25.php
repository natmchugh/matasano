<?php

namespace Matasano;

require 'vendor/autoload.php';

$plaintext = file_get_contents(__DIR__.'/q25_plaintext.txt');

$message = new Message($plaintext);
$key = $message->genKey(16);
$nonce = $message->genKey(8);
$ciphertext = $message->ctr_encrypt($key, $nonce, Message::NONCE);

function edit($ciphertext, $key, $offset, $newtext)
{
    list($nonce, $ciphertext) = explode(':', $ciphertext);
    $message = new Message($ciphertext);
    $plainText = $message->ctr_decrypt($key, $nonce, Message::NONCE);
    $newPlainText = substr_replace($plainText, $newtext, $offset);
    $newPlainText = new Message($newPlainText);
    return $newPlainText->ctr_encrypt($key, $nonce, Message::NONCE);
}

$keyStream = edit($nonce.':'.$ciphertext, $key, 0, str_repeat(chr(0), strlen($ciphertext)));

$xor = new XorEncoder($keyStream, $ciphertext);
echo $xor->encode(),PHP_EOL;


/**
 * Attack the key stream by feeding in blank cipher text to edit function
 * Then just need to xor that with original cipher text and get plain text
 *
 *  Example Output:
 *
 * I'm back and I'm ringin' the bell 
 * A rockin' on the mike while the fly girls yell 
 * In ecstasy in the back of me 
 * Well that's my DJ Deshay cuttin' all them Z's 
 * Hittin' hard and the girlies goin' crazy 
 * Vanilla's on the mike, man I'm not lazy. 
 * ...
 */

