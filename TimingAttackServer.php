<?php

// easier to use than version used for other questions
require(__DIR__.'/SHA1.php');
/**
 * Server to run timing attacks against
 * Usage: php -S localhost:8000 TimingAttackServer.php
 * 
 */


// time between compares in ms
$sleepyTime = 2;

/** 
 * HMAC function from wikipedia psedo code
 */
function hmac($key, $message)
{
    $blocksize = 64;
    if (strlen($key) > $blocksize) {
        $key = hash_sha1($key, true); // keys longer than blocksize are shortened
    }
     // keys shorter than blocksize are zero-padded (where ∥ is concatenation)
    while ($blocksize > strlen($key)) {
        $key .= chr(0);
    }
    $o_key_pad = $i_key_pad = '';
    foreach (str_split($key) as $keyChr) {
        $o_key_pad .= chr(0x5c ^ ord($keyChr)); // Where blocksize is that of the underlying hash function
        $i_key_pad .= chr(0x36 ^ ord($keyChr)); // Where ⊕ is exclusive or (XOR)
    }
    return sha1($o_key_pad . sha1($i_key_pad . $message, true)); // Where ∥ is concatenation
}
/**
 * Compare byte by byte with a pause after comprison
 */
function insecure_compare($a, $b, $sleepyTime) {
    foreach (str_split($a) as $i => $chrA) {
        if ($chrA != substr($b, $i, 1)) {
            return false;
        }
        usleep($sleepyTime * 1000);
    }
    return true;
}

$file = $_GET['file'];
$signature = $_GET['signature'];


$key = 'YELLOW SUBMARINE';
if (insecure_compare($signature, hmac($key, $file), $sleepyTime)) {
    die('HMAC is good thanks for the file.');
}

http_response_code(500);
die('Uhuh HMAC looks like crap not taking your file.');