<?php

namespace Matasano;

require 'vendor/autoload.php';

function MT19937Encrypt($inputText, $key)
{
    $mt = new MersenneTwister($key & 0x00ffff);
    $output = array();
    foreach (str_split($inputText) as $chr) {
        $keyChr = $mt->extract_number() & 0x0000ff;
        $output[] = chr(ord($chr) ^ $keyChr);
    }
    return implode($output);
}

function MT19937Decrypt($cypherText, $key)
{
    $mt = new MersenneTwister($key & 0x00ffff);
    $output = array();
    foreach (str_split($cypherText) as $chr) {
        $keyChr = $mt->extract_number() & 0x0000ff;
        $output[] = chr(ord($chr) ^ $keyChr);
    }
    return implode($output);
}
 
$chosenPlainText = str_repeat('A', 14);

for ($i = 0; $i < mt_rand(1,16) ; $i++) {
    $randChr = chr(mt_rand(0,255));
    $chosenPlainText = $randChr.$chosenPlainText;
}

$key = mt_rand(0, 65025);
$ct = MT19937Encrypt($chosenPlainText, $key);

function getKeyStream($plainText, $cypherText)
{
    $keyStream = array();
    foreach (str_split($plainText) as $offset => $chr) {
        $keyStream[] = ord($chr) ^ ord(substr($cypherText, $offset,1));
    }
    return $keyStream;
}

$keyStream = getKeyStream($chosenPlainText, $ct);

$possibleKeys = range(0x000000, 0x00ffff);

function findKey($possibleKeys, $keyStream) {
    foreach ($possibleKeys as $key) {
        $mt = new MersenneTwister($key & 0x00ffff);
        foreach ($keyStream as $chr) {
            $keyOrd = $mt->extract_number() & 0x0000ff;
            if ($keyOrd != $chr) {
                continue 2;
            }
        }
        return $key;
    }
}

$key = findKey($possibleKeys, $keyStream);
$pt = MT19937Decrypt($ct, $key);

function createPasswordResetToken()
{
    $mt = new MersenneTwister(time());
    $token = '';
    for ($i =0; $i < 4; $i++) {
        $randInt = $mt->extract_number();
        $token .= dechex($randInt);
    }
    return $token;
}

function guessSeed($token)
{
    for ($seedGuess= time(); $seedGuess > 0 ; $seedGuess--) {
        $mt = new MersenneTwister($seedGuess);
        foreach (str_split($token, 8) as $part) {
            $hex = $mt->extract_number();
            if ($hex !== dechex($part)) {
                return $seedGuess;
            }
        }

    }
}
$token = createPasswordResetToken();
$seed = guessSeed($token);
var_dump($key, $pt, $token, $seed);

/*
Example Output:

int(63610)
string(17) "?AAAAAAAAAAAAAA"
string(32) "e37c3cb5ecd26f4c517e378b331f65cd"
int(1395059042)
 */
