<?php

namespace Matasano;

require 'vendor/autoload.php';

// valid padding 4 x 04
$paddedString = 'ICE ICE BABY'.str_repeat(chr(4), 4);
$message = new Message($paddedString);
$message = $message->stripPks7pad($paddedString);
var_dump((string) $message);

// invalid padding 5 x 04
$paddedString = 'ICE ICE BABY'.str_repeat(chr(5), 4);
$message = new Message($paddedString);
try {
    $message = $message->stripPks7pad($paddedString);
} catch (\InvalidArgumentException $e) {
    var_dump($e->getMessage());
}
// invalid padding 01020304
$paddedString = 'ICE ICE BABY'.chr(1).chr(2).chr(3).chr(4);
$message = new Message($paddedString);
try {
    $message = $message->stripPks7pad($paddedString);
} catch (\InvalidArgumentException $e) {
    var_dump($e->getMessage());
}

/*
ouput is
string(12) "ICE ICE BABY"
string(15) "Padding invalid"
string(15) "Padding invalid"
*/