#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>


void baby_step_giant_step (mpz_t g, mpz_t h, mpz_t p, mpz_t n, mpz_t x ){
   unsigned long int i;
   long int j = 0;
   mpz_t N;
   mpz_t* gr ; /* list g^r */
   unsigned long int* indices; /* indice[ i ] = k <=> gr[ i ] = g^k */
   mpz_t hgNq ; /* hg^(Nq) */
   mpz_t inv ; /* inverse of g^(N) */
   mpz_init (N) ;
   mpz_sqrt (N, n ) ;
   mpz_add ui (N, N, 1 ) ;
 
   gr = malloc (mpz_get_ui (N) * sizeof (mpz t) ) ;
   indices = malloc ( mpz_get_ui (N) * sizeof (long int ) ) ;
   mpz_init_set_ui (gr[ 0 ], 1);
 
   /* find the sequence {g^r} r = 1 ,.. ,N (Baby step ) */
   for ( i = 1 ; i <= mpz get ui (N) ; i++) {
      indices[i - 1] = i - 1 ;
      mpz_init (gr[ i ]) ;
      mpz_mul (gr[ i ], gr[ i - 1 ], g ); /* multiply gr[i - 1] for g */
      mpz_mod (gr[ i ], gr[ i ], p );
   }
   /* sort the values (k , g^k) with respect to g^k */
   quicksort ( gr, indices, 0, mpz_get_ui (N) ) ;
   /* on calcule g^(-Nq)   (Giant step) */
   mpz_init_set (inv, g);
   mpz_powm (inv, inv, N, p);  /* inv <- inv ^ N (mod p)  */
   inverse (inv, p, inv) ;
 
   mpz_init_set (hgNq, h);
 
   /* find the elements in the two sequences */
   for ( i = 0 ; i <= mpz get ui (N) ; i++){
      /* find hgNq in the sequence gr ) */
      j = binary_search (gr, hgNq, 0, mpz_get_ui (N) ) ;
      if ( j >= 0 ){
         mpz_mul_ui (N, N, i);
         mpz_add_ui (N, N, indices [j]);
         mpz_set (x, N) ;
         return;
      }
      /* if j < 0, find the next value of g^(Nq) */
      mpz_mul (hgNq, hgNq, inv);
      mpz_mod (hgNq, hgNq, p);
   }
}