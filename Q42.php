<?php

namespace Matasano;

require 'vendor/autoload.php';

$rsa = new RSA();
$message = 'hi mom';

$hash = hash('sha1', $message, true);


$zeros = str_repeat(chr(0), 85);
$padded = $rsa->padBlock($hash.$zeros);

$forgedBlock = new Message($padded);
$forgedBlockDecimal = $forgedBlock->toDecimal();

$root = NumberTheory::cubeRoot($forgedBlockDecimal);
$perfectCube = bi_pow($root, 3);
$signature = new Message($root, Message::DECIMAL);

var_dump($rsa->checkSignature((string) $signature, $message));
