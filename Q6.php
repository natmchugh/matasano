<?php

namespace Matasano;

require 'vendor/autoload.php';

$url  = 'https://gist.github.com/tqbf/3132752/raw/cecdb818e3ee4f5dda6f0847bfd90a83edb87e73/gistfile1.txt';
$content = file_get_contents($url);
$message = new Message($content, Message::BASE64);

$possibleKeySize = range(2, 40);

$normHammings = array();
$analyser = new Analyser();
foreach ($possibleKeySize as $keySize) {
    $chunks = $message->getChunks($keySize);
    $normHamming = array();
    for ($i = 1; $i < 15; $i++) {
        $hamming = $analyser->hamming($chunks[0], $chunks[$i]);
        $normHamming[] = $hamming;
    }
    $normHammings[$keySize] = (array_sum($normHamming)/$keySize);
}
asort($normHammings);
$keySizes = array_keys($normHammings);
$keySizes = array_slice($keySizes, 0, 1);
foreach ($keySizes as $keySize) {
    $chunks = $message->getChunks($keySize);
    $blocks = $message->transposeChunks($chunks);
    $xorKey = '';
    foreach ($blocks as $block) {
        $phrase = new Message();
        $phrase->setOrds($block);
        $encoder = new XorEncoder($phrase);
        $xorKey .= $encoder->getBestKey();
    }
    if (!empty($xorKey)) {
        $encoder = new XorEncoder($message, $xorKey);
        echo "Key size $keySize",PHP_EOL;
        echo "Key is: $xorKey",PHP_EOL;
        echo $encoder->decode();
    }
}
