<?php

namespace Matasano;

require 'vendor/autoload.php';

$url  = 'https://gist.github.com/tqbf/3132928/raw/6f74d4131d02dee3dd0766bd99a6b46c965491cc/gistfile1.txt';
$handle = fopen($url, 'r');
$count = 0;
while (($buffer = fgets($handle, 4096)) !== false) {
    $message = new Message(trim($buffer));
    $analyser = new Analyser($message);
    ++$count;
    if ($analyser->containsDuplicates()) {
        var_dump($count);
    }
}
