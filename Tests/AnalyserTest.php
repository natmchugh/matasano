<?php

namespace Matasano\Test;

use Matasano\Message;
use Matasano\Analyser;

class AnalyserTest extends \PHPUnit_Framework_TestCase
{

    public function testContainsDuplicatesTrue()
    {
        $text = str_repeat('0123456789ABCDEF', 20);
        $key = 'YELLOW SUBMARINE';
        $encrypt = openssl_encrypt($text, 'AES-128-ECB', $key);
        $message = new Message($encrypt, Message::BASE64);
        $analyser = new Analyser($message);
        $this->assertTrue($analyser->containsDuplicates());
    }

    public function testHamming()
    {
        $analyser = new Analyser();
        $hamming = $analyser->hamming(str_split('toned'), str_split('roses'));
        $this->assertSame(3, $hamming);
    }

    public function testHammingEmail()
    {
        $string1 = '0111010001101000011010010111001100100000011010010111001100100000011000010010000001110100011001010111001101110100';
        $string2 = '0111011101101111011010110110101101100001001000000111011101101111011010110110101101100001001000010010000100100001';

        $analyser = new Analyser();
        $hamming = $analyser->hamming(str_split($string1), str_split($string2));
        $this->assertSame(37, $hamming);
    }

    public function testIsEnglish()
    {
        $lyrics = <<< ICE
Burning 'em, if you ain't quick and nimble
I go crazy when I hear a cymbal
ICE;
        $message = new Message($lyrics);
        $analyser = new Analyser($message);
        $this->assertTrue($analyser->isEnglish());
    }

    public function testIsEnglishFalse()
    {
        $lyrics = <<< ICE
dsg sd;ogjojlvds 0hisd7 cxmsdfouds sdodssdlsd
ICE;
        $message = new Message($lyrics);
        $analyser = new Analyser($message);
        $this->assertFalse($analyser->isEnglish());
    }

    public function testContainsDuplicatesFalse()
    {
        $text = str_repeat('0123456789ABCDEF', 20);
        $key = 'YELLOW SUBMARINE';
        $encrypt = openssl_encrypt($text, 'AES-128-CBC', $key, 0, "1234567812345678");
        $message = new Message($encrypt);
        $analyser = new Analyser($message);
        $this->assertFalse($analyser->containsDuplicates());
    }
}