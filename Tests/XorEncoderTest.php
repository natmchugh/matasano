<?php

namespace Matasano\Test;

use Matasano\Message;
use Matasano\XorEncoder;

class XorEncoderTest extends \PHPUnit_Framework_TestCase
{
    public function testXOREncoded()
    {
$lyrics = <<< ICE
Burning 'em, if you ain't quick and nimble
I go crazy when I hear a cymbal
ICE;
        $message = New message($lyrics);
        $xorEncoder = new xorEncoder($message, 'ICE');
        $actual = $xorEncoder->encode()->toHex();
        $expected = "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f";
        $this->assertSame($expected, $actual);
    }

    public function testXORDecode()
    {
$lyrics = <<< ICE
Burning 'em, if you ain't quick and nimble
I go crazy when I hear a cymbal
ICE;

$encoded = "CzY3JyorLmNiLC5paSojaToqPGMkIC1iPWM0PComImMkJydlJyooKy8gQwplLixlKjEkMzplPisgJ2MMaSsgKDFlKGMmMC4nKC8=";
        $message =  New message($encoded, Message::BASE64);
        $xorEncoder = new xorEncoder($message, 'ICE');
        $actual = $xorEncoder->encode();
        $this->assertSame($lyrics, (string) $actual);
    }

    public function testGetBestKey()
    {
        $hex = '1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736';
        $message = New message($hex, Message::HEX);
        $xorEncoder = new xorEncoder($message);
        $this->assertSame('X', $xorEncoder->getBestKey());
    }
}