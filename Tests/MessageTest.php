<?php

namespace Matasano\Test;

use Matasano\Message;
use Matasano\Base64Message;
use Matasano\HexMessage;

class MessageTest extends \PHPUnit_Framework_TestCase
{
    public function testFactoryDefault()
    {
        $message = new Message('test');
        $this->assertInstanceOf('Matasano\Message', $message);
    }

    public function testHex()
    {
        $message = new Message('49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d', Message::HEX);
        $this->assertSame("I'm killing your brain like a poisonous mushroom", (string) $message);
    }

    public function testBase64()
    {
        $message = new Message('SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t', Message::BASE64);
        $this->assertSame("I'm killing your brain like a poisonous mushroom", (string) $message);
    }

    public function testTransposeChunks()
    {
        $chunks = array(
            range('a', 'c'),
            range('d', 'f'),
            range('g','i'),
        );
        $message = new Message();
        $expected = array(
            ['a', 'd', 'g'],
            ['b', 'e', 'h'],
            ['c', 'f', 'i'],
        );
        $this->assertSame($expected, $message->transposeChunks($chunks));
    }

    public function testPKS7Padding()
    {
        $message = New message('YELLOW SUBMARINE');
        $paddedMessages = $message->pks7pad(20);
        $paddedMessage = $paddedMessages[0];
        $this->assertContains('04040404', $paddedMessage->toHex());
    }

    public function testStripPKS7Padding()
    {
        $paddedBlock = New message('59454c4c4f57205355424d4152494e4504040404', MESSAGE::HEX);
        $message = $paddedBlock->stripPks7pad();
        $this->assertSame('YELLOW SUBMARINE', (string) $message);
    }

    public function testStripPKS7PaddingFullBlock()
    {
        $paddedBlock = New message('59454c4c4f57205355424d4152494e4510101010101010101010101010101010', MESSAGE::HEX);
        $message = $paddedBlock->stripPks7pad();
        $this->assertSame('YELLOW SUBMARINE', (string) $message);
    }
    
    public function testPKS7PaddingMultipleOfBlocks()
    {
        $message = New message('YELLOW SUBMARINE');
        $paddedMessages = $message->pks7pad(16);
        $paddedMessage = $paddedMessages[1];
        $this->assertSame('10101010101010101010101010101010', $paddedMessage->toHex());
    }

    public function testPKS7Padding255()
    {
        $message = New message('Y');
        $paddedMessages = $message->pks7pad(256);
        $paddedMessage = $paddedMessages[0];
        $this->assertContains('ff', $paddedMessage->toHex());
    }

    public function testPKS7PaddingMultiBlock()
    {
        $message = New message('YELLOW SUBMARINE');
        $paddedMessages = $message->pks7pad(9);
        $paddedMessage = $paddedMessages[1];
        $this->assertContains('0202', $paddedMessage->toHex());
    }

    public function testCBCEncryption()
    {
        $key = 'YELLOW SUBMARINE';
        $text = 'YELLOW SUBMARINES';
        $iv = str_repeat('aa', 8);
        $expected = openssl_encrypt($text, 'AES-128-CBC', $key, 0, $iv);
        $message = new Message($text);
        $actual = $message->cbc_encrypt($key, $iv);
        $this->assertSame($expected, base64_encode($actual));
    }

    public function testCBCDecryption()
    {
        $key = 'YELLOW SUBMARINE';
        $text = 'YELLOW SUBMARINES';
        $iv = str_repeat('aa', 8);
        $message = new Message($text);
        $cypherText = base64_encode($message->cbc_encrypt($key, $iv));
        $cypherMesage = new Message($cypherText, MESSAGE::BASE64);
        $this->assertSame($text, (string) $cypherMesage->cbc_decrypt($key, $iv)->stripPks7pad());
    }

    public function testGenKey()
    {
        $message = New message();
        $key = $message->genKey(16);
        $this->assertTrue(strlen($key) == 16);
    }

    public function testEncryption_oracle()
    {
        $message = New message();
        $cypherText = $message->encryption_oracle('YELLOW SUBMARINE');
        $this->assertTrue(is_string($cypherText));
        $this->assertFalse($cypherText == 'YELLOW SUBMARINE');
    }

    public function testParseKeyValue()
    {
        $message = New message();
        $expected = array(
            'foo' => 'bar',
            'baz' => 'qux',
            'zap'=> 'zazzle',
        );
        $actual = $message->parseKeyValues('foo=bar&baz=qux&zap=zazzle');
        $this->assertSame($expected, $actual);
    }

    public function testParseKeyValueEmailAddress()
    {
        $message = New message();
        $expected = array(
            'email' => 'foo@bar.com',
            'uid' => '10',
            'role' => 'user'
        );
        $actual = $message->parseKeyValues('email=foo@bar.com&uid=10&role=user');
        $this->assertSame($expected, $actual);
    }

    public function testCreateEncodedPairs()
    {
        $message = New message();
        $pairs = array(
            'email' => 'foo@bar.com',
            'uid' => '10',
            'role' => 'user'
        );
        $expected = 'email=foo@bar.com&uid=10&role=user';
        $actual = $message->encodePairs($pairs);
        $this->assertSame($expected, $actual);
    }

    public function testVerifyPadding()
    {
        $paddedString = 'ICE ICE BABY'.str_repeat(chr(4), 4);
        $message = New message($paddedString);
        $message = $message->stripPks7pad($paddedString);
        $this->assertSame('ICE ICE BABY', (string) $message);
    }

    public function testVerifyPaddingException()
    {
        $this->setExpectedException('InvalidArgumentException');
        $paddedString = 'ICE ICE BABY'.str_repeat(chr(5), 4);
        $message = New message($paddedString);
        $message = $message->stripPks7pad($paddedString);
    }

    public function testVerifyPaddingExceptionDifValues()
    {
        $this->setExpectedException('InvalidArgumentException');
        $paddedString = 'ICE ICE BABY'.chr(1).chr(2).chr(3).chr(4);
        $message = New message($paddedString);
        $message = $message->stripPks7pad($paddedString);
    }

    public function testVerifyPaddingExceptionWithNull()
    {
        $this->setExpectedException('InvalidArgumentException');
        $paddedString = 'YELLOWSUBMARINE'.chr(0);
        $message = New message($paddedString);
        $message = $message->stripPks7pad($paddedString);
    }

    public function testVerifyPaddingExceptionBadLength()
    {
        $this->setExpectedException('InvalidArgumentException');
        $paddedString = 'ICE ICE BABY'.str_repeat(chr(4), 3);
        $message = New message($paddedString);
        $message = $message->stripPks7pad($paddedString);
    }

    public function testProfileFor()
    {
        $email = 'foo@bar.com';
        $expected = 'email=foo@bar.com&uid=10&role=user';
        $message = New message('uid=10&role=user');
        $actual = $message->profile_for($email);
        $this->assertSame($expected, $actual);
    }

    public function testProfileForStripMetacharacters()
    {
        $email = 'foo@bar.com&role=admin';
        $expected = 'email=foo@bar.comroleadmin&uid=10&role=user';
        $message = New message('uid=10&role=user');
        $actual = $message->profile_for($email);
        $this->assertSame($expected, $actual);
    }

    public function testPrependAndQuotePrepend()
    {
        $message = New message('test');
        $actual = $message->prependAndQuote();
        $expected = 'comment1=cooking%20MCs;userdata=test';
        $expected .= ';comment2=%20like%20a%20pound%20of%20bacon';
        $this->assertSame($expected, (string)  $actual);
    }

    public function testPrependAndQuoteQuote()
    {
        $message = New message(';=');
        $actual = $message->prependAndQuote();
        $expected = 'comment1=cooking%20MCs;userdata=\;\=';
        $expected .= ';comment2=%20like%20a%20pound%20of%20bacon';
        $this->assertSame($expected, (string) $actual);
    }

    public function testCtrEncrypt()
    {
        $key = 'YELLOWSUBMARINE';
        $iv = New message('f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff', Message::HEX);
        $plainText = 'Anything less than the best is a felony';
        $cipherText = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plainText, 'ctr', $iv);
        $message = New message($plainText);
        $encrypted = $message->ctr_encrypt($key, $iv);
        $this->assertSame($cipherText, (string) $encrypted);
    }

    public function testIncrementBigEndian()
    {
        $message = new Message('Too cold');
        $message->increment();
        $this->assertSame('Too cole', (string) $message);
    }

    public function testIncrementLittleEndian()
    {
        $message = new Message('Too cold');
        $message->incrementLittleEndian();
        $this->assertSame('Uoo cold', (string) $message);
    }

    public function testIncrementWrap()
    {
        $message = new Message('f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff', Message::HEX);
        $message->increment();
        $this->assertSame('f0f1f2f3f4f5f6f7f8f9fafbfcfdff00', $message->toHex());
    }

    public function testIncrementWrapMultiple()
    {
        $message = New message('f0f1f2f3f4f5f6f7f8f9faffffffffff', Message::HEX);
        $message->increment();
        $this->assertSame('f0f1f2f3f4f5f6f7f8f9fb0000000000', $message->toHex());
    }

    public function testCtrDecrypt()
    {
        $key = 'YELLOWSUBMARINE';
        $iv = New message('f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff', Message::HEX);
        $plainText = 'Deadly, when I play a dope melody\nAnything less than the best is a felony';
        $cipherText = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plainText, 'ctr', $iv);
        $message = New message($cipherText);
        $decrypted = $message->ctr_decrypt($key, $iv);
        $this->assertSame($plainText, (string) $decrypted);
    }

    public function testCloneMesssage()
    {
        $m1 = new Message('foo');
        $m2 = clone($m1);
        $m1[2] = ord('d');
        $this->assertSame('fod', (string) $m1, 'Failed to update original');
        $this->assertSame('foo', (string) $m2, 'Updated the clone');
    }

    public function testToDecimal()
    {
        $m = new Message('Test');
        $this->assertSame('1415934836', $m->toDecimal());
    }

    public function testFromDecimal()
    {
        $m = new Message('1415934836', Message::DECIMAL);
        $this->assertSame('Test', (string) $m);
    }

    public function testToFromDecimal()
    {
        $int = '176368846691323818199095062025495403875642355180590033359338129804017374257255211313410525396425965384';
        $m = new Message($int, Message::DECIMAL);
        $this->assertSame($int, $m->toDecimal());
    }

}