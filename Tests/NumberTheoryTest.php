<?php

namespace Matasano\Test;

use Matasano\NumberTheory;


class NumberTheoryTest extends \PHPUnit_Framework_TestCase
{

    public function testInvMod() 
    {
        $nt = new NumberTheory();
        $invmod = $nt->invMod(17, 3120);
        $this->assertSame('2753', $invmod);
    }

    public function testCubeRoot() 
    {
        $nt = new NumberTheory();
        $invmod = $nt->cubeRoot(27);
        $this->assertSame(3, (int) $invmod);
    }

    public function testCubeRootDecimal() 
    {
        $nt = new NumberTheory();
        $this->assertSame('3', $nt->cubeRoot(21));
    }


    public function testCubeRootLarge() 
    {
        $nt = new NumberTheory();
        $crt = $nt->cubeRoot('4054477107000000000');
        $this->assertSame('1594575', $crt);
    }


    public function testCubeRootSuperLarge() 
    {
        $nt = new NumberTheory();
        $crt = $nt->cubeRoot('1881676377413297425320704533203867');
        $this->assertSame('123456789123', $crt);
    }
    
}