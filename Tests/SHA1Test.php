<?php

namespace Matasano\Test;

use Matasano\Message;
use Matasano\SHA1;

class SHA1Test extends \PHPUnit_Framework_TestCase
{
    public function testPreProcess()
    {
        $message = new Message("Hello is it me you're looking for");
        $sha1 = new SHA1($message);
        $this->assertSame(0, strlen($sha1->getMessage()) % 64);
    }

    public function testPreProcessLongData()
    {
        $message = new Message(file_get_contents(__DIR__.'/../set4.txt'));
        $sha1 = new SHA1($message);
        $this->assertSame(strlen($sha1->getMessage()) % 64, 0);
    }

    public function testHash()
    {
        $message = new Message('password');
        $sha1 = new SHA1($message);
        $this->assertSame(sha1('password'), $sha1->hash());
    }

    public function testHashWorkedExample()
    {
        $message = new Message('A Test');
        $sha1 = new SHA1($message);
        $this->assertSame('8f0c0855915633e4a7de19468b3874c8901df043', $sha1->hash());
    }

    public function testHashLongData()
    {
        $message = new Message(file_get_contents(__DIR__.'/../set4.txt'));
        $sha1 = new SHA1($message);
        $this->assertSame(sha1_file(__DIR__.'/../set4.txt'), $sha1->hash());
    }
}