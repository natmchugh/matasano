<?php

require(__DIR__.'/SHA1.php');

function hmac($key, $message)
{
    $blocksize = 64;
    if (strlen($key) > $blocksize) {
        $key = hash_sha1($key, true); // keys longer than blocksize are shortened
    }
     // keys shorter than blocksize are zero-padded (where ∥ is concatenation)
    while ($blocksize > strlen($key)) {
        $key .= chr(0);
    }
    $o_key_pad = $i_key_pad = '';
    foreach (str_split($key) as $keyChr) {
        $o_key_pad .= chr(0x5c ^ ord($keyChr)); // Where blocksize is that of the underlying hash function
        $i_key_pad .= chr(0x36 ^ ord($keyChr)); // Where ⊕ is exclusive or (XOR)
    }
    return sha1($o_key_pad . sha1($i_key_pad . $message, true)); // Where ∥ is concatenation
}

function callService($hmac, $data)
{
    $url = sprintf('http://localhost:8000/?file=%s&signature=%s', urlencode($data), urlencode($hmac));
    $startTime = microtime(true);
    $response =  @file_get_contents($url);
    $time = microtime(true) - $startTime;
    preg_match('#HTTP/\d+\.\d+ (\d+)#', $http_response_header[0], $matches);
    if (200 == (int) $matches[1]) {
        return 99;
    }
    return $time;
}


$dataString = "Turn off the lights and I'll glow";
echo hmac('YELLOW SUBMARINE', $dataString), PHP_EOL;
$possibleChrs = array_merge(range(0, 9), range('a', 'f'));

$times = array();
$noTimesToAverage = 100;
$totalTime = 0;
for($j = 0; $j < $noTimesToAverage; $j++) {
    $hmacToTry = str_repeat('X', 32);
    $time = callService($hmacToTry, $dataString);
    $times[] = $time;
}

for ($j = 0; $j < 40; $j++) {
    $times = array_combine($possibleChrs, array_pad(array(), 16, array()));
    for ($i = 0; $i < $noTimesToAverage; $i++) {
        shuffle($possibleChrs);
        foreach ($possibleChrs as $possibleChr) {
            $hmacToTry = substr_replace($hmacToTry, $possibleChr, $j, 1);
            $time = callService($hmacToTry, $dataString);
            $times[$possibleChr][] = $time;
        }
        usleep($noTimesToAverage);
    }
    $longest = array(0 => 0);
    foreach ($times as $chr => $time) {
        $totalTime = array_sum($time);
        if ($totalTime > current($longest)) {
            $longest = array($chr => $totalTime);
        }
    }
    $chr = key($longest);
    $hmacToTry = substr_replace($hmacToTry, $chr, $j, 1);
    echo $chr;
}
echo PHP_EOL;

/**
 *  Slightly improved algo seems to work ok down to 2ms by randomising 
 *  and upping the number of requests with a little wait after every few requests.
 *  No need for backtrack.
 *
 *  Example Output:
 *
 * 420f16c6a50bf9f2adf8ddc4e7f6d7f948dc7678
 * 420f16c6a50bf9f2adf8ddc4e7f6d7f948dc7678
 */


 
