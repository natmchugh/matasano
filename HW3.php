<?php

$handle = fopen(__DIR__.'/testVideoFile.mp4', 'r');

$blocks = array();
while (!feof($handle)) {
    $blocks[] = fread($handle, 1024);
}

$blocks = array_reverse($blocks);
$lastBlock = array_shift($blocks);
$length = count($blocks) - 1;
$sha256 = hash('sha256', $lastBlock, true);

foreach ($blocks as $i => $block) {
    $frame = sprintf('%s%s', $block, $sha256);
    $sha256 = hash('sha256', $frame, true);
    if ($length === $i) {
        var_dump(hash('sha256', $frame, false));
    }
}

fclose($handle);