<?php

namespace Matasano;

require 'vendor/autoload.php';

function reverseBitshiftLeftXor($y, $shift, $mask) {
  $i = $result = 0;
  $reg = $y;
  while (($i * $shift) < 32) {
    $reg = $reg << $shift;
    $reg = $y ^ ($reg & $mask);
    $i++;
  }
  return $reg;
}

function reverseBitshiftRightXor($y, $shift) {
  $i = $result =0;
  $reg = $y;
  while (($i * $shift) < 32) {
    $reg = $reg >> $shift;
    $reg = $y ^ $reg;
    $i++;
  }
  return $reg;
}

function  untemper($y) {
    $y = reverseBitshiftRightXor($y, 18);
    $y = reverseBitshiftLeftXor($y, 15, 0xefc60000);
    $y = reverseBitshiftLeftXor($y, 7, 0x9d2c5680);
    $y = reverseBitshiftRightXor($y, 11);
    return $y;
}

$mt = new MersenneTwister(time());
$state = array();
for ($i = 0; $i < 624; $i++) {
    $tempered = $mt->extract_number();
    $state[$i] = untemper($tempered);
}

$mt2 = new MersenneTwister('anything');
$mt2->setInternalState($state, 0);

for ($j = 0; $j < 10; $j++) {
    echo 'First MersenneTwister says:',$mt->extract_number(),PHP_EOL;
    echo 'Cloned MersenneTwister says:',$mt2->extract_number(),PHP_EOL;
}

/*
  Example Output:

First MersenneTwister says:222245852
Cloned MersenneTwister says:222245852
First MersenneTwister says:4119580316
Cloned MersenneTwister says:4119580316
First MersenneTwister says:2049554506
Cloned MersenneTwister says:2049554506
First MersenneTwister says:2088574622
Cloned MersenneTwister says:2088574622
First MersenneTwister says:1520792645
Cloned MersenneTwister says:1520792645
First MersenneTwister says:3409056292
Cloned MersenneTwister says:3409056292
First MersenneTwister says:1861802614
Cloned MersenneTwister says:1861802614
First MersenneTwister says:4151174307
Cloned MersenneTwister says:4151174307
First MersenneTwister says:2439325375
Cloned MersenneTwister says:2439325375
First MersenneTwister says:3395092018
Cloned MersenneTwister says:3395092018

 */


