<?php

namespace Matasano;

require 'vendor/autoload.php';


$key = (new Message)->genKey();

function encryptCookie($key)
{
$cookies = array(
    'MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc=',
    'MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic=',
    'MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw==',
    'MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg==',
    'MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl',
    'MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA==',
    'MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw==',
    'MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8=',
    'MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g=',
    'MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93',
);
$index = array_rand($cookies);
$message = new Message($cookies[$index], Message::BASE64);
$iv = $message->genKey(16);
$cypherText = $message->cbc_encrypt($key, $iv);
return $iv.$cypherText;
}


function validPadding($cypherText, $key, $iv)
{
    try {
        $cypherText = new Message($cypherText);
        $cookie = $cypherText->cbc_decrypt($key, $iv);
        $cookie->stripPks7pad();
    } catch (\InvalidArgumentException $e) {
        return false;
    }
    return true;
}

function testPenultimateChr($c1, $c2, $key) {
    $c1Prime = clone($c1);
    // test xor of non null against penutimate chr if still gives valid padding we had 0x01
    $c1Prime[14] = ($c1[14] ^ 0x01);
    return !validPadding($c2, $key, $c1Prime);
}

// get a prioritised list of ords we could decrypt to
function getOrds($c1n)
{
    $priority = array_merge(array(32), range(97, 122), range(65, 96), range(0, 31), range(33, 64), range(123, 255) );
    $ords = array();
    foreach ($priority as $p2) {
        $ords[] = ($p2 ^ (1 ^ $c1n));
    }
    return $ords;
}

$ct = encryptCookie($key);

$blocks = str_split($ct, 16);

$endBockNo = count($blocks)-2;

$plainTexts = array_pad(array(), $endBockNo + 1, array());

foreach ($blocks as $blockNo => $block) {
    $decoded = array();
    $padding = '';
    if (!isset($blocks[$blockNo+1])) {
        break 1;
    }

    $c1 = new Message($block);
    $c1Prime = clone($c1);
    $c2 = new Message($blocks[$blockNo+1]);
    for ($j = 1 ; $j < 17; $j++) {
        $ords = getOrds($c1[16 - $j]);
        foreach ($ords as $i) {
            $c1Prime[16 - $j] = $i;
            $valid = validPadding($c2, $key, $c1Prime.$padding);

            if ($valid)  {
                if ($blockNo == $endBockNo && $j == 1) {
                    if (testPenultimateChr($c1Prime, $c2, $key)) {
                        continue 1;
                    }
                }
                $i2 = ($i ^ $j);
                $lastByte = ($i2 ^ $c1[16 - $j]);
                array_unshift($plainTexts[$blockNo], chr($lastByte));
                array_unshift($decoded, $i2);
                $padding = '';
                foreach ($decoded as $decode) {
                    $padding .= chr(($j + 1) ^ $decode);
                }
                $c1Prime = new Message(substr($c1, 0, 15 - $j));
                break 1;
            }
        }
    }
}

ksort($plainTexts);
foreach ($plainTexts as $plainText) {
    echo implode($plainText);
}
echo PHP_EOL;


/*
    Example Outputs:
    000000Now that the party is jumping
    000001With the bass kicked in and the Vega's are pumpin'
    000002Quick to the point, to the point, no faking
    000003Cooking MC's like a pound of bacon
    000004Burning 'em, if you ain't quick and nimble
    000005I go crazy when I hear a cymbal
    000006And a high hat with a souped up tempo
    000007I'm on a roll, it's time to go solo
    000008ollin' in my five point oh
 */
