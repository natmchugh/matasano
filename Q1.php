<?php

namespace Matasano;

require 'vendor/autoload.php';

$hex = '49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d';

$message = new Message($hex, Message::HEX);
$encoded = base64_encode((string) $message);

echo $encoded,PHP_EOL;
