<?php

namespace Matasano;

require 'vendor/autoload.php';

$url  = 'https://gist.github.com/tqbf/3132976/raw/f0802a5bc9ffa2a69cd92c981438399d4ce1b8e4/gistfile1.txt';
$data = file_get_contents($url);

$key = 'YELLOW SUBMARINE';

$message = new Message($data, Message::BASE64);

$iv = str_repeat(chr(0), 16);
echo $message->cbc_decrypt($key, $iv);

/*
Output is:

I'm back and I'm ringin' the bell 
A rockin' on the mike while the fly girls yell 
In ecstasy in the back of me 
...

 */