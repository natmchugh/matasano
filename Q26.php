<?php

namespace Matasano;

require 'vendor/autoload.php';

$message = new Message();

$key = $message->genKey(16);
$nonce = str_repeat(chr(0), 8);

function encrypt($message, $key, $nonce)
{
    $message = $message->prependAndQuote();
    $message = new Message($message);
    return $message->ctr_encrypt($key, $nonce, Message::NONCE);
}

function isAdmin($message, $key, $nonce)
{
    $decrypted = $message->ctr_decrypt($key, $nonce, Message::NONCE);
    var_dump((string) $decrypted);
    return 1 == preg_match('/;admin=true;/', $decrypted);
}

$message = new Message('test:admin<true');
$encrypted = encrypt($message, $key, $nonce);
$nonce = new Message($nonce);

$fullString = $message->prependAndQuote();

//find position of : which is one byte less than ;
$targetPosition = strpos($fullString, ':admin');
// XOR against difference in bits in plain test 
$encrypted[$targetPosition] = $encrypted[$targetPosition] ^ 1;

//do the same for < to transform it to =
//start in previous block
$targetPosition = strpos($fullString, '<true');
$encrypted[$targetPosition] = $encrypted[$targetPosition] ^ 1;

// should now be admin
echo isAdmin($encrypted, $key, $nonce) ? 'is admin': 'is not admin', PHP_EOL;

/*
Turns out to even easier than CBC as just need to flip bit in same block

Example Output:

string(89) "comment1=cookingE?t????r.j?]test;admin=true;comment2=%20like%20a%20pound%20of%20bacon"
is admin
*/