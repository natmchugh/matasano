<?php

namespace Matasano;

require 'vendor/autoload.php';

$hex = 'bcbf217cb280cf30b2517052193ab979';

$message = new Message($hex, Message::HEX);

$message2 = new Message('66e94bd4ef8a2c3b884cfa59ca342b2e', Message::HEX);

$xor = new XorEncoder($message, $message2);

echo $xor->encode()->toHex(),PHP_EOL;


// f5651d4feb8054f8e01e740756d9e211