<?php

namespace Matasano;

require 'vendor/autoload.php';

$cypherTexts = file(__DIR__.'/HW1_cypherTexts.txt');

$cypherTexts = array_filter($cypherTexts, function($text) {
    return !empty(trim($text));
});

$messages = array();

$cypherTexts = array_values($cypherTexts);

foreach ($cypherTexts as $i => $cypherText) {
    $messages[] = new Message($cypherText, MESSAGE::HEX);
}

$encoded = array();

foreach ($messages as $messageI) {
    foreach ($messages as $messageJ) {
        
        $encoder = new XorEncoder($messageI, $lastMessageJ);
        $encoded[] = $encoder->encode();
    }
}

$asciiLetters = array_merge(array(32), range(64, 90), range(97, 122));

$plainTexts = array_pad(array(), 11, array());

$keyStream = array_pad(array(), 150, 0);

$commonLetters = array(32);

foreach ($commonLetters as $commonLetter) {
    foreach ($encoded as $messageNumber => $encode) {
        foreach ($encode as $position => $ord) {
            $xor = ($ord ^ $commonLetter);
            if (in_array($xor, $asciiLetters)) {
                $possibleKey = ($messages[$messageNumber][$position] ^ $xor);
                foreach ($messages as $message) {
                    if (isset($message[$position])) {
                        $possPlainChr = ($message[$position] ^ $possibleKey);
                        if (!in_array($possPlainChr, $asciiLetters)) {
                            unset($possibleKey);
                            break 1;
                        }
                    }
                }
                if (isset($possibleKey)) {
                    $keyStream[$position] = $possibleKey;
                }
            }
        }
    }
}

$recongisedPhrase = str_split('You don\'t want to buy a set of car keys from a guy who specializes in stealing cars');

foreach ($recongisedPhrase as $position => $plainText) {
    $ord = ord($plainText);
    $keyStream[$position] = $ord ^ $messages[4][$position] ;
}

$key = new Message();
$key->setOrds($keyStream);

foreach ($messages as $i => $message) {
    $encoder = new XorEncoder($message, $key);
    $plainTexts[$i] =(string) $encoder->decode();
}

var_dump($plainTexts);
