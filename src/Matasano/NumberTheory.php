<?php

namespace Matasano;

class NumberTheory
{
    public static function invMod($a, $m)
    {
        list($g, $x, $y) = self::extendedGCD($a, $m);
        return bi_to_str(bi_absmod($x, $m));
    }

    private static function extendedGCD($a, $b)
    {
        $s = 0; $old_s = 1;
        $t = 1; $old_t = 0;
        $r = $b; $old_r = $a;
        while (bi_to_str($r) != 0) {
            $quotient = bi_div($old_r, $r);
            list($old_r, $r) = [$r, bi_sub($old_r, bi_mul($quotient, $r))];
            list($old_s, $s) = [$s, bi_sub($old_s, bi_mul($quotient, $s))];
            list($old_t, $t) = [$t, bi_sub($old_t, bi_mul($quotient, $t))];
        }
        if (bi_to_str($old_r) != 1) {
            throw new \OutOfBoundsException('Not co-prime');
        }
        return [$old_r, $old_s, $old_t];
    }

    public static function cubeRoot($A)
    {
        $x = bi_from_str(1);
        $A = bi_from_str($A);
        $dX = bi_from_str(1);
        while (!bi_is_zero($dX)) {
            $dX = bi_sub(bi_div($A, bi_pow($x, 2)), $x);
            $dX = bi_div($dX, 3);
            $x = bi_add($x, $dX);
        }
        return bi_to_str($x);
    }
}