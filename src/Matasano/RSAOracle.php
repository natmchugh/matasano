<?php

namespace Matasano;

class RSAOracle
{
    private $calls = 0;

    public function __construct($k = 1024)
    {
        $config = array(
            "private_key_bits" => $k,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );
            
        // Create the private and public key
        $res = openssl_pkey_new($config);

        // Extract the private key from $res to $privKey
        openssl_pkey_export($res, $this->privKey);

        // Extract the public key from $res to $pubKey
        $this->pubKey = openssl_pkey_get_details($res);
    }

    public function getPublicKey()
    {
        $e = bi_base_convert(bin2hex($this->pubKey['rsa']['e']), 16, 10);
        $n = bi_base_convert(bin2hex($this->pubKey['rsa']['n']), 16, 10);
        return [$e, $n];
    }

    public function oracle($encrypted)
    {
        $decrypted = null;
        ++$this->calls;
        // decrypt ignoring padding for now will fail if padding is broke
        openssl_private_decrypt($encrypted, $decrypted, $this->privKey, OPENSSL_NO_PADDING);
        return (0 === strpos(bin2hex($decrypted), '0002'));
    }
    
    public function encrypt($message)
    {
        openssl_public_encrypt($message, $c, $this->pubKey['key']);
        return $c;
    }

    public function getCalls()
    {
        return $this->calls;
    }
}