<?php

namespace Matasano;

class DSA
{

    private $p;

    private $q;

    private $g;

    private $x;

    private $y;

    public function __construct($p, $q, $g)
    {
        $this->p = $p;
        $this->q = $q;
        $this->g = $g;
    }

    public function setPrivateKey($x)
    {
        $this->x = $x;
    }

    public function setPublicKey($y)
    {
        $this->y = $y;
    }

    public function genKeys()
    {
        $this->x = bi_rand(128);
        //y = g^x mod p
        $this->y = bi_powmod($this->g, $this->x, $this->p);
    }

    public function getPublicKey()
    {
        return [$this->p, $this->q, $this->g, $this->y];
    }

    public function recoverPrivateKey($k, $r, $s, $message, $q)
    {
       //    (s * k) - H(msg)
       // x = ----------------  mod q
       //             r
        $hash = hash('sha1', $message);
        $H = bi_base_convert($hash, 16, 10);
        return bi_divmod(bi_sub(bi_mul($s, $k), $H), $r, $q);
    }

    public function sign($message, $k = 0) 
    {
        $hash = hash('sha1', $message);
        $H = bi_base_convert($hash, 16, 10);
        do {
            if (empty($k)) {
                $k = bi_rand(128);
            }
            $kInv = NumberTheory::invMod($k, $this->q);
            // r= (g^k mod p) mod q
            $r = bi_mod(bi_powmod($this->g, $k, $this->p), $this->q);
            // s=k^-1(H(m)+x  mod q
            $s = bi_mulmod($kInv, bi_add($H, bi_mul($this->x, $r)), $this->q);
            
        } while($k == 0 || $s == 0);
        return [$r, $s];
    }

    public function verify($message, $r, $s)
    {
        if ($r < 0 || $r > $this->q || $s < 0 || $s > $this->q) {
            return false;
        }
        $hash = hash('sha1', $message);
        $H = bi_base_convert($hash, 16, 10);
        $w = NumberTheory::invMod($s, $this->q);
        $u1 = bi_mulmod($H, $w, $this->q);
        $u2 = bi_mulmod($r, $w, $this->q);
        $v = bi_mod(
            bi_mulmod(bi_powmod($this->g, $u1, $this->p),
            bi_powmod($this->y, $u2, $this->p), $this->p),
            $this->q
        );
        return bi_to_str($v) ==  bi_to_str($r);
    }

}