<?php

namespace Matasano;

function bi_ceil_div($a, $b)
{
    $base = bi_to_str(bi_div($a, $b));
    $modulus = bi_to_str(bi_mod($a, $b)) == 0 ? 0 : 1;
    return bi_to_str(bi_add($base, $modulus));
}

function bi_floor_div($a, $b)
{
    return bi_div($a, $b);
}

function bi_max($a, $b)
{
    return bi_cmp($a, $b) == 1? $a : $b;
}

function bi_min($a, $b)
{
    return bi_cmp($a, $b) == -1? $a : $b;
}

function bi_range($start, $finish, $step = 1)
{
    $pos = $start;
    while (1 > bi_cmp($pos, $finish)) {
        yield $pos;
        $pos = bi_add($pos, $step);
    }
}