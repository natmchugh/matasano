<?php

namespace Matasano;

class Analyser
{
    private $message;

    private $score = 0;

    public function __construct($message = '')
    {
        $this->message = $message;

    }

    public function getScore()
    {
        return $this->score;
    }

    public function getTopTen($counts)
    {
        $topTen = array('n','a', 'e', 'i', 'o', 't', 'r', 's', 'h', ' ');
        $topTenCount = 0;
        foreach ($topTen as $chr) {
            $ord = ord($chr);
            if (isset($counts[$ord]))
            $topTenCount += $counts[$ord];
        }
        return $topTenCount / array_sum($counts);
    }

    public function hamming($a, $b)
    {
        $difference = array_diff_assoc(
            $a,
            $b
        );
        return count($difference);
    }

    public function isEnglish()
    {
        $counts = array_count_values($this->message->getArrayCopy());
        if ($counts)
        $this->score = $this->getTopTen($counts);
        return  $this->score > 0.5;
    }

    public function containsDuplicates()
    {
        $chunks = $this->message->getChunks(16);
        $blocks = [];
        foreach ($chunks as $chunk) {
            $blocks[] = implode($chunk);
        }
        $counts = array_count_values($blocks);
        return count($counts) < count($blocks);
    }
}