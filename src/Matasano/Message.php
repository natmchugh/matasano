<?php

namespace Matasano;


class Message extends \ArrayIterator
{
    const PLAIN = 0;

    const HEX = 1;

    const BASE64 = 2;

    const DECIMAL = 4;

    const DECIMAL_CHARS = 32;

    const NONCE = 8;

    const IV = 16;


    public function __construct($string ='', $encoding = Message::PLAIN)
    {
        switch ($encoding) {
            case (Message::HEX):
                $this->fromHex($string);
                break;
            case (Message::DECIMAL):
                $this->fromDecimal($string);
                break;
                break;
            case (Message::BASE64):
                $string = base64_decode($string);
            default:
                parent::__construct(array_values(unpack('C*', $string)));
        }
    }


    public function __clone()
    {
         return parent::__construct($this->getArrayCopy());
    }

    public function fromHex($hexString)
    {
        foreach (str_split($hexString, 2) as $i => $pair) {
            $this->offsetSet($i, hexdec($pair));
        }
    }

    public function fromDecimal($decString, $len = 2)
    {
        $hex = bi_base_convert($decString, 10, 16);
        $length = ceil(strlen($hex) / 2) * 2;
        $hex = str_pad($hex, $length, '0', STR_PAD_LEFT);
        $this->fromHex($hex);
    }

    public function increment()
    {
        $end = count($this) -1;
        $lsi = end($this);
        $this[$end] = ++$lsi;
        $carry = 0;
        for ($i = $end; $i > 0; $i--) {
            $register = $this->offsetGet($i);
            $register += $carry;
            $this->offsetSet($i, $register % 256);
            $carry = floor($register / 256);
        }
        return $this;
    }

    public function incrementLittleEndian()
    {
        $end = count($this) -1;
        $msi = $this[0];
        $this[0] = ++$msi;
        $carry = 0;
        for ($i = 0; $i > $end; $i++) {
            $register = $this->offsetGet($i);
            $register += $carry;
            $this->offsetSet($i, $register % 256);
            $carry = floor($register / 256);
        }
        return $this;
    }

    public function setOrds($ords)
    {
        foreach ($ords as $i => $ord) {
            $this->offsetSet($i, $ord);
        }
    }

    public function __toString()
    {
        $string = '';
        foreach ($this as $byte) {
            $string .= chr($byte);
        }
        return $string;
    }

    public function getChunks($size)
    {
        return array_chunk($this->getArrayCopy(), $size);
    }

    public function toHex()
    {
        $hex = '';
        foreach ($this as $ord) {
            $hex .= sprintf('%02x', $ord);
        }
        return $hex;
    }

    public function toBase64()
    {
        return base64_encode($this);
    }

    public function toDecimal()
    {
        return bi_to_str(bi_base_convert($this->toHex(), 16, 10));
    }

    public function toBinaryString()
    {
        $bin = '';
        foreach ($this as $ord) {
            $bin .= sprintf('%08b', $ord);
        }
        return $bin;
    }

    public function transposeChunks($chunks)
    {
        $blocks = array();
        foreach ($chunks as $chrs) {
            foreach ($chrs as $j => $chr) {
                if (isset($blocks[$j])) {
                    $blocks[$j][] = $chr;
                } else {
                    $blocks[$j] = [$chr];
                }
            }
        }
        return $blocks;
    }

    public function pks7pad($blockLength)
    {
        $chunks = $this->getChunks($blockLength);
        $end = count($chunks)-1;
        $k = $blockLength - count($chunks[$end]);
        if (0 === $k) {
            $k = $blockLength;
            ++$end;
            $chunks[] = array();
        }
        for ($i = 0; $i < $k; $i++) {
            $chunks[$end][] = $k;
        }
        $blocks = array();
        foreach ($chunks as $chunk) {
            $msg = new Message();
            $msg->setOrds($chunk);
            $blocks[] = $msg;
        }
        return $blocks;
    }

    public function stripPks7pad() 
    {
        $lastOrd = end($this);
        if ($lastOrd < 1 || $lastOrd > 17) {
            throw new \InvalidArgumentException('Padding invalid');
        }
        $length = count($this) - 1;
        $ords = $this->getArrayCopy();
        for ($i = 0;  $i < $lastOrd; $i++) {
            $popped = array_pop($ords);
            if ($popped != $lastOrd) {
                throw new \InvalidArgumentException('Padding invalid');
            }
        }
        $message = new Message();
        $message->setOrds($ords);
        return $message;
    }

    public function cbc_encrypt($key, $iv)
    {

        $blocks = $this->pks7pad(16);
        $cypertext = $iv;
        foreach ($blocks as $i => $block) {
            $message = new Message($block);
            $encoder = new XorEncoder($message, $cypertext);
            $cypertext = openssl_encrypt($encoder->encode(), 'AES-128-ECB', $key, OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING);
            $blocks[$i] = $cypertext;
        }
        return new Message(implode($blocks));
    }

    public function cbc_decrypt($key, $iv)
    {
        $blocks = str_split($this, 16);
        $cypertext = $iv;
        foreach ($blocks as $i => $block) {
            $decrypted = openssl_decrypt($block, 'AES-128-ECB', $key, OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING);
            $encoder = new XorEncoder((new Message($decrypted)), $cypertext);
            $cypertext = $block;
            $blocks[$i] = $encoder->decode();
        }
        $message = new Message(implode($blocks));
        // $cleanMsg = $message->stripPks7pad();
        return $message;
    }

    public function ctr_encrypt($key, $nonce, $mode = Message::IV)
    {

        $blocks = str_split($this, 16);
        $counter = 0;
        $ctr = '';
        if ($mode == Message::NONCE) {
            $ctr = new Message(str_repeat(chr(0), 8));
        }
        foreach ($blocks as $i => $block) {
            $pad = openssl_encrypt($nonce.$ctr, 'AES-128-ECB', $key, OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING);
            $encoder = new XorEncoder(new Message($block), new Message($pad));
            $blocks[$i] = $encoder->encode();
            if ($mode == Message::NONCE) {
                $ctr->incrementLittleEndian();
            } else {
                $nonce->increment();
            }
        }
        return new Message(implode($blocks));
    }

    public function ctr_decrypt($key, $iv, $mode = Message::IV)
    {
        return $this->ctr_encrypt($key, $iv, $mode);
    }

    public function genKey($length = null)
    {
        if (empty($length)) {
            $length = rand(8,32);
        }
        $key = '';
        for ($i = 0; $i < $length; $i++) {
            $key .= chr(mt_rand(0, 255));
        }
        return $key;
    }

    public function encryption_oracle($input)
    {
        $preFix = $this->genKey(mt_rand(5, 10));
        $postFix = $this->genKey(mt_rand(5, 10));
        $key = $this->genKey(16);
        $plaintext = $preFix.$input.$postFix;
        if (1 == rand(1,2)) {
            $cypertext = openssl_encrypt($plaintext, 'AES-128-ECB', $key, OPENSSL_RAW_DATA);
        } else {
            $message = New message($plaintext);
            $iv = $this->genKey(16);
            $cypertext = (string) $message->cbc_encrypt($key, $iv);
        }
        return $cypertext;
    }

    public function ecb_encryption_oracle_prefix($input, $prefix)
    {
        $key = base64_decode('0UY8q59jmQkS11aEwCS+Tw==');
$postFix = <<< BASE64
Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg
aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq
dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg
YnkK
BASE64;
        $plaintext = $prefix.$input.base64_decode($postFix);
        $cypertext = openssl_encrypt($plaintext, 'AES-128-ECB', $key, OPENSSL_RAW_DATA);
        return $cypertext;
    }

    public function ecb_encryption_oracle($input)
    {
        $key = base64_decode('0UY8q59jmQkS11aEwCS+Tw==');
$postFix = <<< BASE64
Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg
aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq
dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg
YnkK
BASE64;
        $plaintext = $input.base64_decode($postFix);
        $cypertext = openssl_encrypt($plaintext, 'AES-128-ECB', $key, OPENSSL_RAW_DATA);
        return $cypertext;
    }

    public function parseKeyValues($query)
    {
        $params = array();
        foreach (explode('&', $query) as $chunk) {
            $param = explode("=", $chunk);
            $params[$param[0]] = $param[1];
        }
        return $params;
    }

    public function encodePairs($pairs)
    {
        $encoded = '';
        foreach ($pairs as $key => $value) {
                $encoded .= sprintf('%s=%s&', $key, $value);
        }
        return trim($encoded, '&');
    }

    public function profile_for($email)
    {
        $profile = $this->parseKeyValues((string) $this);
        $cleanerEmail = str_replace(array('&', '='), '', $email);
        $profile = array_merge(array('email' => $cleanerEmail), $profile);
        return $this->encodePairs($profile);
    }

    public function prependAndQuote()
    {
        $prefix = 'comment1=cooking%20MCs;userdata=';
        $postfix = ';comment2=%20like%20a%20pound%20of%20bacon';
        $string =  sprintf('%s%s%s', $prefix, addcslashes($this, '=;'), $postfix);
        return New message($string);
    }

    public function doubleExponent($exponent, $modulus)
    {
        $doubleExponent = bi_powmod(2, $exponent, $modulus);
        $c = bi_mulmod($this->toDecimal(), $doubleExponent, $modulus);
        return new Message(bi_to_str($c), Message::DECIMAL);
    }
}
