<?php

namespace Matasano;

class MersenneTwister
{
    
 // Create a length 624 array to store the state of the generator
    private $MT;
    private $index;
     
    public function __construct($seed)
    {
        $this->initialize_generator($seed);
    }

    public function setInternalState($state, $index)
    {
        $this->MT = $state;
        $this->index = $index;
    }

     // Initialize the generator from a seed
     function initialize_generator($seed) {
         $this->MT = new \SplFixedArray(624);
         $this->index = 0;
         $this->MT[0] = $seed;
         for ($i = 1; $i < 624; $i++) { // loop over each other element
             $this->MT[$i] =  (0x6c078965 * ($this->MT[$i-1] ^ ($this->MT[$i-1] >> 30)) + $i) & 0xffffffff;// 0x6c078965
         }
     }
     
     // Extract a tempered pseudorandom number based on the index-th value,
     // calling generate_numbers() every 624 numbers
     function extract_number() {
         if ($this->index == 0) {
             $this->generate_numbers();
         }

         $y = $this->MT[$this->index];
         $y = $y ^ ($y >> 11);
         $y = $y ^ (($y << 7) & 0x9d2c5680); // 0x9d2c5680
         $y = $y ^ (($y << 15) & 0xefc60000); // 0xefc60000
         $y = $y ^ ($y >> 18);

         $this->index = ($this->index + 1) % 624;
         return $y;
     }
     
     // Generate an array of 624 untempered numbers
     function generate_numbers() {
         foreach ($this->MT as $i => $mt) {
             $y = ($mt & 0x80000000) + ($this->MT[($i+1) % 624] & 0x7fffffff);   // bits 0-30 (first 31 bits) of MT[...]
             $this->MT[$i] = $this->MT[(($i + 397) % 624)] ^ ($y >> 1);
             if (($y % 2) != 0) { // y is odd
                 $this->MT[$i] = $mt ^ 0x9908b0df; // 0x9908b0df
             }
         }
     }

}