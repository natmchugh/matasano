<?php
//
// Naive hand rolled RSA implementation
namespace Matasano;

class RSA{

    private $n;

    private $d;

    private $e;

    private $blockSizeBytes = 128;

    public function __construct()
    {
        $config = array(
            "digest_alg" => "sha1",
            "private_key_bits" => 1024,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );
            
        // Create the private and public key
        $res = openssl_pkey_new($config);

        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);

        $this->e = bi_base_convert(bin2hex($pubKey['rsa']['e']), 16, 10);
        $this->n = bi_base_convert(bin2hex($pubKey['rsa']['n']), 16, 10);
        $this->d = bi_base_convert(bin2hex($pubKey['rsa']['d']), 16, 10);
    }

    public function getPublicKey()
    {
        //Your public key is [e, n]. Your private key is [d, n].
        return [$this->e, $this->n];
    }

    public function encrypt($plainText) {
        $message = new Message($plainText);
        // To encrypt: c = m**e%n. To decrypt: m = c**d%n
        return bi_powmod($message->toDecimal(), $this->e, $this->n);
    }

    public function getSignature($plainText)
    {
        $hash = hash('sha1', $plainText, true);
        $padded = $this->padBlock($hash);
        $c = $this->decrypt($padded);
        return bi_to_str($c);
    }

    public function padBlock($text)
    {
        $goop = new Message('3021300906052b0e03021a05000414', Message::HEX);
        $asn1 = $goop.$text;
        $psLength = $this->blockSizeBytes - strlen($asn1) - 3;
        $ps = str_repeat(chr(0xff), $psLength);
        return sprintf('%c%c%s%c%s', 0x00, 0x01, $ps, 0x00, $asn1);
    }

    public function checkSignature($signature, $plainText)
    {
        $signature = $this->encrypt($signature);
        $signature = new Message(bi_to_str($signature), Message::DECIMAL);
        $hash = hash('sha1', $plainText);
        // look for expected hash and ans1 signifier via reg ex
        $ans1 = '3021300906052b0e03021a05000414';
        if (!preg_match("/01(ff+)00$ans1$hash/", $signature->toHex(), $matches)) {
            return false;
        }
        return true;
    }
    
    public function decrypt($c)
    {
        return bi_powmod($c, $this->d, $this->n);
    }
}

