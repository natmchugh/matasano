<?php

namespace Matasano;

class MD4
{
    public function __construct($message) {
        $this->registers = [0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476];
        $originalSize = strlen($message) * 8;
        $message .= chr(128);
        while (((strlen($message) + 8) % 64) !== 0) {
            $message .= chr(0);
        }
        $masks = [0x00000000ffffffff, 0xffffffff00000000];
        foreach ($masks as $mask) {
            $ords = unpack('C*' , pack('L', $originalSize & $mask));
            foreach ($ords as $ord) {
                $message .= chr($ord);
            }
        }
        $this->message = $message;
    }

    public function getMessage() {
        return $this->message;
    }

    public function rotl($x, $n) {
        return ($x << $n) | ($x >> (32 - $n));
    }

    public function f($x, $y, $z) {
        return ($x & $y) | (~$x & $z);
    }

    public function g($x, $y, $z)
    {
        return ($x & $y) | ($x & $z) | ($y & $z);
    }

    public function h($x, $y, $z)
    {
        return $x ^ $y ^ $z;
    }

    public function setInitalRegisters(Array $registers) {
        $this->registers = $registers;
    }

    public function setMessage($message) {
        $this->message = $message;
    }


    public function hash() {
        list($a, $b, $c, $d) = $this->registers;
        $message = $this->message;
        // Process the message in successive 512-bit chunks:
        // break message into 512-bit chunks
        $chunks = str_split($message, 64);
        foreach ($chunks as $chunk) {
            // break chunk into sixteen 32-bit big-endian words w[i], 0 ≤ i ≤ 15
            $words = str_split($chunk, 4);
            foreach ($words as $i => $chrs) {
                $chrs = str_split($chrs);
                $word = '';
                $chrs = array_reverse($chrs);
                foreach ($chrs as $chr) {
                    $word .= sprintf('%08b', ord($chr));
                }
                $words[$i] = bindec($word);
            }
            list($aa, $bb, $cc, $dd) = [$a, $b, $c, $d];
            foreach ([0, 4, 8, 12] as $i) {
                $a = $this->rotl($a + $this->f($b, $c, $d) + $words[$i+0] & 0xffffffff, 3);
                $d = $this->rotl($d + $this->f($a, $b, $c) + $words[$i+1] & 0xffffffff, 7);
                $c = $this->rotl($c + $this->f($d, $a, $b) + $words[$i+2] & 0xffffffff, 11);
                $b = $this->rotl($b + $this->f($c, $d, $a) + $words[$i+3] & 0xffffffff, 19) ;
            }
            foreach ([0, 1, 2, 3] as $i) {
                $a = $this->rotl($a + $this->g($b, $c, $d) + $words[$i+0] + 0x5a827999 & 0xffffffff, 3);
                $d = $this->rotl($d + $this->g($a, $b, $c) + $words[$i+4] + 0x5a827999 & 0xffffffff, 5);
                $c = $this->rotl($c + $this->g($d, $a, $b) + $words[$i+8] + 0x5a827999 & 0xffffffff, 9);
                $b = $this->rotl($b + $this->g($c, $d, $a) + $words[$i+12] + 0x5a827999 & 0xffffffff, 13);
            }
            foreach ([0, 2, 1, 3] as $i) {
                $a = $this->rotl($a + $this->h($b, $c, $d) + $words[$i+0] + 0x6ed9eba1 & 0xffffffff, 3);
                $d = $this->rotl($d + $this->h($a, $b, $c) + $words[$i+8] + 0x6ed9eba1 & 0xffffffff, 9);
                $c = $this->rotl($c + $this->h($d, $a, $b) + $words[$i+4] + 0x6ed9eba1 & 0xffffffff, 11);
                $b = $this->rotl($b + $this->h($c, $d, $a) + $words[$i+12] + 0x6ed9eba1 & 0xffffffff, 15);
            }
            $a = $a + $aa & 0xffffffff;
            $b = $b + $bb & 0xffffffff;
            $c = $c + $cc & 0xffffffff;
            $d = $d + $dd & 0xffffffff;
        }
        $x = pack('V4', $a, $b, $c, $d);
        return bin2hex($x);
    }
}