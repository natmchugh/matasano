<?php
//
// Naive hand rolled RSA implementation
namespace Matasano;

class RSA{

    private $n;

    private $d;

    private $e = 3;

    private $blockSizeBytes = 128;

    public function __construct()
    {
         do {
            // - Generate 2 random primes. We'll use small numbers to start, so you
            //  can just pick them out of a prime table. Call them "p" and "q".
            $rand = $this->bigRandomNumber();
            $p = bi_next_prime($rand);
            $rand = $this->bigRandomNumber();
            $q = bi_next_prime($rand);
            // - Let n be p * q. Your RSA math is modulo n.
            $this->n = bi_mul($p, $q); 
            // 
            // - Let et be (p-1)*(q-1) (the "totient"). You need this value only for
            //  keygen.

            $et = bi_mulmod(bi_sub($p, 1) , bi_sub($q, 1), $this->n);

            $coPrime = true;
            try {
                // - Compute d = invmod(e, et). invmod(17, 3120) is 2753.
                $this->d = NumberTheory::invMod($this->e, $et);
            } catch (\OutOfBoundsException $exception) {
                $coPrime = false;
            }
        } while (!$coPrime);
    }

    public function bigRandomNumber() {
        $bytes = openssl_random_pseudo_bytes($this->blockSizeBytes);
        return bi_base_convert(bin2hex($bytes), 16, 10);
    }

    public function getPublicKey()
    {
        //Your public key is [e, n]. Your private key is [d, n].
        return [$this->e, $this->n];
    }

    public function encrypt($plainText) {
        $message = new Message($plainText);
        // To encrypt: c = m**e%n. To decrypt: m = c**d%n
        return bi_powmod($message->toDecimal(), $this->e, $this->n);
    }

    public function getSignature($plainText)
    {
        $hash = hash('sha1', $plainText, true);
        $padded = $this->padBlock($hash);
        $c = $this->decrypt($padded);
        return bi_to_str($c);
    }

    public function padBlock($text)
    {
        $goop = new Message('3021300906052b0e03021a05000414', Message::HEX);
        $asn1 = $goop.$text;
        $psLength = $this->blockSizeBytes - strlen($asn1) - 3;
        $ps = str_repeat(chr(0xff), $psLength);
        return sprintf('%c%c%s%c%s', 0x00, 0x01, $ps, 0x00, $asn1);
    }

    public function checkSignature($signature, $plainText)
    {
        $signature = $this->encrypt($signature);
        $signature = new Message(bi_to_str($signature), Message::DECIMAL);
        $hash = hash('sha1', $plainText);
        // look for expected hash and ans1 signifier via reg ex
        $ans1 = '3021300906052b0e03021a05000414';
        if (!preg_match("/01(ff+)00$ans1$hash/", $signature->toHex(), $matches)) {
            return false;
        }
        return true;
    }
    
    public function decrypt($c)
    {
        $m = bi_powmod($c, $this->d, $this->n);
        $m = bi_to_str($m, 16);
        $length = ceil(strlen($m) / 2) * 2;
        $m = str_pad($m, $length, '0', STR_PAD_LEFT);
        return $m;
    }
}

