<?php

namespace Matasano;

class XorEncoder
{
    private $key;

    private $message;

    public function __construct(Message $message, $key = '')
    {
        $this->message = $message;
        $this->setKey($key);
    }

    public function encode()
    {
        $count = 0;
        $encode = new Message();
        foreach ($this->message as $ord) {
            $mod = $count % count($this->key);
            $keyChr = $this->key[$mod];
            $encode->offsetSet($count, ord(chr($ord) ^ $keyChr));
            ++$count;
        }
        return $encode;
    }

    public function add()
    {
        $count = 0;
        $encode = new Message();
        foreach ($this->message as $ord) {
            $mod = $count % count($this->key);
            $keyChr = $this->key[$mod];
            $encode->offsetSet($count, ($ord + ord($keyChr)) % 256);
            ++$count;
        }
        return $encode;
    }

    public function decode()
    {
        return $this->encode();;
    }

    public function setKey($key)
    {
        $this->key = str_split($key);;
    }

    public function getBestKey()
    {
        $messages = array();
        $posKeys = array();
        $asciiLetters = array_merge(array(32), range(65, 90), range(97, 122));
        for ($keyOrd = 0; $keyOrd < 255; $keyOrd++) {
            foreach ($this->message as $ord) {
                $xor =  ($ord ^ $keyOrd);
                if (in_array($xor, $asciiLetters)) {
                    if (isset($posKeys[$keyOrd])) {
                        $posKeys[$keyOrd]++;
                    } else {
                        $posKeys[$keyOrd] = 1;
                    }
                }
            }
        }   
        $highest = array(0,0);
        foreach ($posKeys as $possKeyChr => $score) {
            if ($score > $highest[0]) {
                $highest = array($score, $possKeyChr);
            }
        }
        return chr($highest[1]);
    }
}