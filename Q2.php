<?php

namespace Matasano;

require 'vendor/autoload.php';

$message = new Message('1c0111001f010100061a024b53535009181c', Message::HEX);
$message2 = new Message('686974207468652062756c6c277320657965', Message::HEX);
$xorEncoder = new xorEncoder($message, (string) $message2);
$xor = $xorEncoder->encode();
echo $xor->toHex(),PHP_EOL;
